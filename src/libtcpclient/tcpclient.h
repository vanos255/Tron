#ifndef TCPCLIENT_H
#define TCPCLIENT_H
#include <QObject>
#include <QString>
#include <QTcpSocket>
#include <QJsonObject>
#include <QJsonDocument>

class Tcpclient : public QObject
{
    Q_OBJECT
public:
    Tcpclient(QObject *parent = 0);
    ~Tcpclient();
signals:
    /**
     * @brief вызывается при успешном подключении к серверу
     */
    void connected();
    /**
     * @brief вызывается при отключении от сервера
     */
    void disconnected();
    /**
     * @brief вызывается при поступлении сообщения от сервера
     * @param message сообщение в виде json объекта
     */
    void incommingMessage(QJsonObject message);
public slots:
    /**
     * @brief подключиться к серверу. без вызова этого метода объект бесполезен
     * @param address адрес сервера
     * @param port порт
     */
    void connectToServer(QString address, int port);

    /**
     * @brief отключиться от сервера
     */
    void disconnectFromServer();

    /**
     * @brief посылает серверу запрос на присоединение к игре
     * @param name имя игрока
     * @param color цвет игрока в hex формате, например #ffffff
     */
    void sendJoinRequest(QString name, QString color);

    /**
     * @brief посылает серверу запрос на переход в категорию зрителей
     */
    void sendSpectateRequest();

    /**
     * @brief посылает запрос на смену состояние игры
     * @param gameState на сервере обрабатываются только "running" и "pause"
     */
    void sendGameStateChangeRequest(QString gameState);

    /**
     * @brief посылает запрос на смену направления
     * @param direction "up","down","left","right"
     */
    void sendChangeDirectionRequest(QString direction);

    /**
     * @brief посылает запрос на получение всей доступной игровой информации.
     */
    void sendStatusRequest();
private slots:
    void onConnected();
    void onDisconnected();
    void onReadyRead();
private:
    QTcpSocket* socket;

    void sendJsonToServer(QJsonObject json);
};

#endif // TCPCLIENT_H
