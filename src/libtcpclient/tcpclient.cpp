#include "tcpclient.h"


Tcpclient::Tcpclient(QObject *parent) : QObject(parent)
{
    socket = nullptr;
}

Tcpclient::~Tcpclient(){
    disconnectFromServer();
}

void Tcpclient::connectToServer(QString address, int port){
    socket = new QTcpSocket(this);

    connect(socket, SIGNAL(connected()),this, SLOT(onConnected()));
    connect(socket, SIGNAL(disconnected()),this, SLOT(onDisconnected()));
    connect(socket, SIGNAL(readyRead()),this, SLOT(onReadyRead()));

    socket->connectToHost(address,port);

    if(!socket->waitForConnected(5000))
    {
        qDebug() << "Error: " << socket->errorString();
    }
}

void Tcpclient::disconnectFromServer()
{
    if (socket!=nullptr){
        socket->abort();
    }
}

void Tcpclient::sendJoinRequest(QString name, QString color)
{
    QJsonObject message;
    message["messageType"]="joinRequest";
    QJsonObject playerInfo;
    playerInfo["name"]=name;
    playerInfo["color"]=color;
    message["player"]=playerInfo;
    sendJsonToServer(message);
}

void Tcpclient::sendSpectateRequest()
{
    QJsonObject message;
    message["messageType"]="spectateRequest";
    sendJsonToServer(message);
}

void Tcpclient::sendGameStateChangeRequest(QString gameState)
{
    QJsonObject message;
    message["messageType"]="gameStateChangeRequest";
    message["gameState"]=gameState;
    sendJsonToServer(message);
}

void Tcpclient::sendChangeDirectionRequest(QString direction)
{
    QJsonObject message;
    message["messageType"]="changeDirectionRequest";
    message["direction"]=direction;
    sendJsonToServer(message);
}

void Tcpclient::sendStatusRequest()
{
    QJsonObject message;
    message["messageType"]="statusRequest";
    sendJsonToServer(message);
}

void Tcpclient::onConnected()
{
    emit connected();
}

void Tcpclient::onDisconnected()
{
    emit disconnected();
    socket->deleteLater();
    socket=nullptr;
}

void Tcpclient::onReadyRead()
{
    if (QTcpSocket *con = qobject_cast<QTcpSocket*>(sender())){
        QByteArray incomming = con->readAll();
        QByteArrayList messages = incomming.split(';');
        foreach (const QByteArray& message, messages) {
            QJsonDocument jsonDocument = QJsonDocument::fromJson(message);
            if (jsonDocument.isNull()){
                return;
            }
            emit incommingMessage(jsonDocument.object());
        }
    }
}

void Tcpclient::sendJsonToServer(QJsonObject json)
{
    if (socket==nullptr || socket->state()!=QTcpSocket::ConnectedState){
        qDebug() << "Необходимо сначала подключиться";
        return;
    }
    QByteArray bytes = QJsonDocument(json).toJson(QJsonDocument::Compact);
    bytes.append(';');
    socket->write(bytes);
    socket->flush();
}
