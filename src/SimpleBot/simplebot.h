#ifndef SIMPLEBOT_H
#define SIMPLEBOT_H
#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QVector>
#include "QTime"
#include <QCoreApplication>
#include "../libtcpclient/tcpclient.h"

struct Cell{
    int ownerId;
    bool isEmpty;
};

class SimpleBot : public QObject
{
    Q_OBJECT
public:
    explicit SimpleBot(QString address, int port, QObject *parent = 0);

signals:

public slots:
    void onIncommingMessage(QJsonObject message);
    void onDisconnect();
    void handleStatusMessage(QJsonObject message);
private:
    QString getNextDirection();
    void getNextLocation(int& nextRow, int& nextColumn);
    bool isCellAllowable(int row, int column);

    Tcpclient* client;
    QVector< QVector<Cell> > map;
    int myId;
    QString myDirection;
    int myRow;
    int myColumn;
    int rows;
    int columns;
    bool amIDead;
};

#endif // SIMPLEBOT_H
