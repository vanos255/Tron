#include <QCoreApplication>
#include <QCommandLineParser>
#include <simplebot.h>
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCommandLineParser parser;
    parser.addHelpOption();

    parser.addPositionalArgument("address", QCoreApplication::translate("main", "address of server. Default is localhost"));
    parser.addPositionalArgument("port", QCoreApplication::translate("main", "port of server. Default is 3000"));

    parser.process(a);

    const QStringList args = parser.positionalArguments();

    QString address="127.0.0.1";
    int port=3000;

    if (!args.isEmpty()){
        address=args.at(0);
        port=QString(args.at(1)).toInt();
    }
    SimpleBot bot(address, port);

    return a.exec();
}
