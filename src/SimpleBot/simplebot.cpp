#include "simplebot.h"

SimpleBot::SimpleBot(QString address, int port, QObject *parent) : QObject(parent)
{
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));

    client=new Tcpclient();
    connect(client,SIGNAL(incommingMessage(QJsonObject)),this,SLOT(onIncommingMessage(QJsonObject)));
    connect(client,SIGNAL(disconnected()),this,SLOT(onDisconnect()));
    client->connectToServer(address,port);
    client->sendJoinRequest("Пашка","");
}

void SimpleBot::onIncommingMessage(QJsonObject message)
{
    if (message["messageType"]=="status"){
        handleStatusMessage(message);
        if (amIDead){
            return;
        }
        int nextRow;
        int nextColumn;
        getNextLocation(nextRow,nextColumn);
        if (!isCellAllowable(nextRow,nextColumn)){
            QString direction = getNextDirection();
            qDebug()<<"Возможно столкновение в "<<nextRow<<" "<<nextColumn<<" пойду в "<<direction;
            client->sendChangeDirectionRequest(direction);
        }
        else
            client->sendChangeDirectionRequest(myDirection);
    } else if (message["messageType"]=="nextTurn" || message["messageType"]=="gameStateChanged") {
        client->sendStatusRequest();
    }
}

void SimpleBot::onDisconnect()
{
    QCoreApplication::quit();
}

void SimpleBot::handleStatusMessage(QJsonObject message)
{
    int rows = message["map"].toObject()["rows"].toInt();
    int columns = message["map"].toObject()["columns"].toInt();

    this->rows=rows;
    this->columns=columns;

    this->myId=message["client"].toObject()["playerId"].toInt();

    map.resize(rows);
    for (auto &row : map) {
        row.resize(columns);
        for (auto &cell : row) {
            cell.isEmpty=true;
        }
    }
    foreach (const QJsonValue& value, message["players"].toArray()) {
        QJsonObject player = value.toObject();
        int row = player["location"].toObject()["y"].toInt();
        int column = player["location"].toObject()["x"].toInt();
        int id = player["id"].toInt();
        if (id==this->myId){
            this->myRow=row;
            this->myColumn=column;
            this->myDirection=player["direction"].toString();
            this->amIDead=player["isDead"].toBool();
        }
        if (player["isDead"].toBool()==false){
        map[row][column].ownerId=id;
        map[row][column].isEmpty=false;
        foreach (const QJsonValue& value, player["trail"].toArray()) {
            QJsonObject trail = value.toObject();
            int trailRow = trail["y"].toInt();
            int trailColumn = trail["x"].toInt();
            map[trailRow][trailColumn].isEmpty=false;
            map[trailRow][trailColumn].ownerId=id;
        }
        }
    }
}

QString SimpleBot::getNextDirection()
{
    QVector<QString> allowableDirections;
    if (isCellAllowable(this->myRow,this->myColumn+1)){
        allowableDirections.append(QString("right"));
    }
    if (isCellAllowable(this->myRow,this->myColumn-1)){
        allowableDirections.append(QString("left"));
    }
    if (isCellAllowable(this->myRow+1,this->myColumn)){
        allowableDirections.append(QString("up"));
    }
    if (isCellAllowable(this->myRow-1,this->myColumn)){
        allowableDirections.append(QString("down"));
    }

    if (allowableDirections.size()==0){
        return this->myDirection;
    }
    return allowableDirections[qrand() % allowableDirections.size()];
}

void SimpleBot::getNextLocation(int &nextRow, int &nextColumn)
{
    if (this->myDirection=="up"){
        nextRow=this->myRow+1;
        nextColumn=this->myColumn;
    }
    if (this->myDirection=="down"){
        nextRow=this->myRow-1;
        nextColumn=this->myColumn;
    }
    if (this->myDirection=="left"){
        nextRow=this->myRow;
        nextColumn=this->myColumn-1;
    }
    if (this->myDirection=="right"){
        nextRow=this->myRow;
        nextColumn=this->myColumn+1;
    }
}

bool SimpleBot::isCellAllowable(int row, int column)
{
    if (row<0 || row>this->rows-1 || column<0 || column>this->columns-1){
        return false;
    }
    return map[row][column].isEmpty;
}
