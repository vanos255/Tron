#include "mainwindow.h"
#include "../libtcpclient/tcpclient.h"
#include "clientgamemodel.h"

#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Tcpclient tcpclient;
    ClientGameModel gameModel;
    MainWindow mainWindow;


    // Window-Client-Server
    // Receiving message from server
    QObject::connect(&tcpclient, SIGNAL(incommingMessage(QJsonObject)), &gameModel, SLOT(ParseMessageSlot(QJsonObject)));

    // Game status reguesting
    QObject::connect(&mainWindow, SIGNAL(StatusSignal()), &gameModel, SLOT(StatusSlot()));
    QObject::connect(&gameModel, SIGNAL(StatusSignal()), &tcpclient, SLOT(sendStatusRequest()));

    // Changing direction
    QObject::connect(&mainWindow, SIGNAL(ChangeDirectionSignal(int)), &gameModel, SLOT(ChangeDirectionSlot(int)));
    QObject::connect(&gameModel, SIGNAL(ChangeDirectionSignal(QString)), &tcpclient, SLOT(sendChangeDirectionRequest(QString)));

    // Connecting to server
    QObject::connect(&mainWindow, SIGNAL(ConnectSignal(QString,int)) , &gameModel, SLOT(ConnectSlot(QString,int)));
    QObject::connect(&gameModel, SIGNAL(ConnectSignal(QString,int)), &tcpclient, SLOT(connectToServer(QString,int)));

    // Disconnecting from server
    QObject::connect(&mainWindow, SIGNAL(DisconnectSignal()) , &gameModel, SLOT(DisconnectSlot()));
    QObject::connect(&gameModel, SIGNAL(DisconnectSignal()), &tcpclient, SLOT(disconnectFromServer()));

    // Choosing role of client: player/spectator
    QObject::connect(&mainWindow, SIGNAL(PlayerInfoSignal(QString,QString,bool)), &gameModel, SLOT(PlayerInfoSlot(QString,QString,bool)));
    QObject::connect(&gameModel, SIGNAL(PlayerJoinSignal(QString,QString)), &tcpclient, SLOT(sendJoinRequest(QString,QString)));
    QObject::connect(&gameModel, SIGNAL(SpectatorSignal()), &tcpclient, SLOT(sendSpectateRequest()));

    // Changing game state
    QObject::connect(&mainWindow, SIGNAL(GameStateSignal()), &gameModel, SLOT(GameStateSlot()));
    QObject::connect(&gameModel, SIGNAL(GameStateSignal(QString)), &tcpclient, SLOT(sendGameStateChangeRequest(QString)));


    // Model-Window
    // Initialisation of game settings
    QObject::connect(&gameModel, SIGNAL(Initialised(Position, QList<PlayerInfo>, int, int)), &mainWindow, SLOT(Initialised(Position, QList<PlayerInfo>, int, int)));
    QObject::connect(&gameModel, SIGNAL(NextTurn(QList<PlayerInfo>)), &mainWindow, SLOT(NextTurn(QList<PlayerInfo>)));

    // Changing game state
    QObject::connect(&gameModel, SIGNAL(GameStateChanged(QString)), &mainWindow, SLOT(GameSrtateChanged(QString)));

    // Role confirmation
    QObject::connect(&gameModel, SIGNAL(RoleConfirmation(bool)), &mainWindow, SLOT(RoleConfirmation(bool)));

    // Get Connected/Disconnected status
    QObject::connect(&tcpclient, SIGNAL(connected()), &gameModel, SLOT(ConnectedStatusSlot()));
    QObject::connect(&tcpclient, SIGNAL(disconnected()), &gameModel, SLOT(DisconnectedStatusSlot()));
    QObject::connect(&gameModel, SIGNAL(ConnectionStatus(bool)), &mainWindow, SLOT(ConnectionStatus(bool)));

    // Sending logs to window
    QObject::connect(&gameModel, SIGNAL(FillLogLIst(QStringList)), &mainWindow, SLOT(FillLogList(QStringList)));


    mainWindow.show();

    return a.exec();
}
