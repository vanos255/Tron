#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include <QObject>

class GameModel : public QObject
{
    Q_OBJECT
public:
    explicit GameModel(QObject *parent = 0);
    Cell **map;


    GameModel();
    GameModel(QList<PlayerInfo> players, Position mapSize);

    ~GameModel();

    void SetMyID(int id);
    int GetMyID();

    void SetIsPlayer(bool isPlayer);
    bool GetIsPlayer();

    void SetGameState(QString gameState);
    QString GetGameState();

    void SetMapChanges(QList<Cell> changes);
    QList<Cell> GetMapChanges();

    void SetPlayersInfo(QList<PlayerInfo> players);
    QList<PlayerInfo> GetPlayersInfo();

    void AddPlayer(PlayerInfo player);
    void RemovePlayer(int id);

    void Initialization(int myID, bool isPlayer, QString gameState, Position mapSize, QList<PlayerInfo> players);
    void Step(QList<PlayerInfo> changes);

private:
    int myId;
    bool isPlayer;
    QString gameState;

    Position mapSize;
    QList<PlayerInfo> players;


    void MessageStatus(QJsonObject message);
    void MessageNextTurn(QJsonObject message);
    void MessagePlayerJoined(QJsonObject message);

signals:
    // to tcpclient
    void ConnectSignal(QString address, int port);
    void DisconnectSignal();
    void PlayerJoinSignal(QString name, QString color);
    void SpectatorSignal();
    void GameStateSignal(QString gameState);
    void StatusSignal();
    void ChangeDirectionSignal(QString direction);

    // to window
    void Initialised(/* mapSize, players */);
    void NextTurn(/* playersChanges */);


    void SetGameInfo();
public slots:
    // from window
    void ConnectSlot(QString address, int port);
    void DisconnectSlot();
    void PlayerInfoSlot(QString name, QString color, bool isPlayer);
    void GameStateSlot(QString gameState);
    void StatusSlot(); // ?
    void ChangeDirectionSlot(QString direction);

    // from tcpclient
    void ParseMessageSlot(QJsonObject initializationInfo);

};

#endif // GAMEMODEL_H
