#ifndef STRUCTURES_H
#define STRUCTURES_H

#include <QList>
#include <QString>

struct Position {
  int x;
  int y;
  Position() {
      x = 0;
      y = 0;
  }
  Position(int x, int y){
      this->x = x;
      this->y = y;
  }
};

enum CellState {
    Empty,
    Player,
    Tail,
};


struct Cell {
    Position position;
    CellState state;
    Cell() {
        position = Position();
        state = Empty;
    }

    Cell(Position position, CellState state) {
        this->position = position;
        this->state = state;
    }
};

struct PlayerInfo {
    int id;
    QString name;
    Position position;
    QString direction;
    QString color;
    bool isDead;
    QList<Cell> tail;

    PlayerInfo(){}
    PlayerInfo(int id, QString name, Position position, QString direction, QString color, bool isDead,  QList<Cell> tail ) {
        this->id = id;
        this->name = name;
        this->position = position;
        this->direction = direction;
        this->color = color;
        this->isDead = isDead;
        this->tail = tail;
    }
};

struct DrawingInfo {
    Cell cell;
    QString cellColor;

    DrawingInfo(){}
    DrawingInfo(Cell cell, QString color){
        this->cell = cell;
        this->cellColor = color;
    }
};


#endif // STRUCTURES_H
