#include "clientgamemodel.h"

ClientGameModel::ClientGameModel(QObject *parent) : QObject(parent)
{
    mapSize = Position(5,5);
    gameLog.AboutGameState("lobby");
}

ClientGameModel::~ClientGameModel() {}

void ClientGameModel::SetMyID(int id)
{
    myId = id;
}

int ClientGameModel::GetMyID()
{
    return myId;
}

void ClientGameModel::SetIsPlayer(bool isPlayer)
{
    this->isPlayer = isPlayer;
}

bool ClientGameModel::GetIsPlayer()
{
    return isPlayer;
}

void ClientGameModel::SetGameState(QString gameState)
{
    this->gameState = gameState;
}

QString ClientGameModel::GetGameState()
{
    return gameState;
}

void ClientGameModel::SetPlayersInfo(QList<PlayerInfo> players)
{
    this->players = players;
}

QList<PlayerInfo> ClientGameModel::GetPlayersInfo()
{
    QList<PlayerInfo> playersInfo = players;
    return playersInfo;
}

void ClientGameModel::AddPlayer(PlayerInfo player)
{
    players.push_back(player);
}

void ClientGameModel::RemovePlayer(int id)
{
    for (int i = 0; i < players.count(); i++) {
        if (players[i].id = id) {
            players.removeAt(i);
            break;
        }
    }
}

void ClientGameModel::Initialization(int myID, bool isPlayer, QString gameState, Position mapSize, QList<PlayerInfo> players)
{
    this->myId = myID;
    this->isPlayer = isPlayer;
    this->gameState = gameState;
    this->mapSize = mapSize;

    GameStateChanged(gameState);
    RoleConfirmation(isPlayer);
    SetPlayersInfo(players);
}

void ClientGameModel::Step(QList<PlayerInfo> changes)
{
    for (int i = 0; i < players.count(); i++) {
        for (int j = 0; j < changes.count(); j++) {
            if (players[i].id == changes[j].id) {
                // Пишем в лог, что проиграл
                if (!players[i].isDead && changes[j].isDead)
                    gameLog.AboutDeadPlayers(players[i].name + "#" + QString::number(players[i].id));

                players[i].position = changes[j].position;
                players[i].direction = changes[j].direction;
                players[i].isDead = changes[j].isDead;
                players[i].tail.append(changes[j].tail);
            }
        }
    }
}

void ClientGameModel::FindWinner()
{
    bool isFound = false;
    for (int i = 0; i < players.count(); i++) {
        if (!players[i].isDead) {
            gameLog.AboutWinner(players[i].name + "#" + QString::number(players[i].id));
            isFound = true;
        }
    }
    if (!isFound)
        gameLog.AboutWinner("NOT DEFINED");
}

/* Инициализация при получении JSON пакета.
 * ----------------------------------------
 * Из пакета получаем данные.
 * Парсим документ в зависимости от типа сообщения.
 *
*/
void ClientGameModel::ParseMessageSlot(QJsonObject message)
{
    QString messageType = "";
    messageType = message.value("messageType").toString();

    if (messageType == "status")
        MessageStatus(message);

    if (messageType == "nextTurn")
        MessageNextTurn(message);

    if (messageType == "playerJoined") {
        StatusSignal();
        MessagePlayerJoined(message);
    }

    if (messageType == "playerLeaved") {
        int leavedPlayerID = message.value("player").toObject().value("id").toInt();
        for (int i = 0; i < players.count(); i++)
            if (players[i].id == leavedPlayerID) {
                gameLog.AboutPlayerConnection(players[i].name + "#" + QString::number(players[i].id), false);
            }
        StatusSignal();
    }
    if (messageType == "joinAcknowledge") {
        bool isPlayer = true;
        int myID = message.value("client").toObject().value("playerId").toInt();

        SetIsPlayer(isPlayer);
        SetMyID(myID);

        RoleConfirmation(isPlayer);
    }
    if (messageType == "spectateAcknowledge")
    {
        bool isPlayer = false;
        SetIsPlayer(isPlayer);
        RoleConfirmation(isPlayer);
    }
    if (messageType == "gameStateChanged")
    {
        QString gState = message.value("gameState").toString();

        if (gameState == "lobby" || gameState == "gameover")
            if (gState == "running")
                StatusSignal();

        gameLog.AboutGameState(gState);

        SetGameState(gState);
        GameStateChanged(gState);
        if (gState == "gameover") {
            FindWinner();
        }
    }

    FillLogLIst(gameLog.GetLogInfo());
}

void ClientGameModel::ConnectedStatusSlot()
{
    isConnected = true;
    ConnectionStatus(isConnected);

    gameLog.AboutConnection(isConnected, address, port);
}

void ClientGameModel::DisconnectedStatusSlot()
{
    isConnected = false;
    ConnectionStatus(isConnected);

    gameLog.AboutConnection(isConnected, address, port);

    FillLogLIst(gameLog.GetLogInfo());
}

/* Обрабоска сообщения от сервера типа "status"
 * --------------------------------------------
 * Парсим полученные данные о карте, игроках, состоянии игры и роли клиента
*/
void ClientGameModel::MessageStatus(QJsonObject message)
{
    QJsonObject client = message.value("client").toObject();
    bool isPlayer = client.value("isPlayer").toBool();
    int playerId = client.value("playerId").toInt();

    QString state = message.value("gameState").toString();

    QList<PlayerInfo> players;
    QJsonArray playersArray = message.value("players").toArray();
    for (int i = 0; i < playersArray.count(); i++) {

        PlayerInfo player;
        int id = playersArray.at(i).toObject().value("id").toInt();
        QString name = playersArray.at(i).toObject().value("name").toString();
        QString direction = playersArray.at(i).toObject().value("direction").toString();
        QJsonObject location = playersArray.at(i).toObject().value("location").toObject();
        int x = location.value("x").toInt();
        int y = location.value("y").toInt();
        QString color = playersArray.at(i).toObject().value("color").toString();
        bool isDead = playersArray.at(i).toObject().value("isDead").toBool();

        QList<Cell> tail;
        QJsonArray trail = playersArray.at(i).toObject().value("trail").toArray();
        for (int j = 0; j < trail.count(); j++) {
            int t_x = trail.at(j).toObject().value("x").toInt();
            int t_y = trail.at(j).toObject().value("y").toInt();

            tail.push_back(Cell(Position(t_x, t_y), Tail));
        }
        Position pos;
        pos.x = x;
        pos.y = y;
        player = PlayerInfo(id, name, pos, direction, color, isDead, tail);
        players.push_back(player);
    }
    QJsonObject map = message.value("map").toObject();
    int rows = map.value("rows").toInt();
    int columns = map.value("columns").toInt();

    int maxPlayers = message.value("maxPlayers").toInt();
    this->maxPlayers = maxPlayers;

    // Инициализируем модель
    Initialization(playerId, isPlayer, state, Position( columns, rows), players);
    Initialised(this->mapSize, this->players, this->myId, this->maxPlayers);
}

/* Обработка сообщения от сервера типа "nextTurn"
 * ----------------------------------------------
 * Парсим полученную информацию о новой позиции игроков,
 * изменении в их хвостах и в игре ли игрок.
*/
void ClientGameModel::MessageNextTurn(QJsonObject message)
{
    QList<PlayerInfo> playersChanges;

    QJsonArray playersArray = message.value("players").toArray();
    for (int i = 0; i < playersArray.count(); i++) {
        PlayerInfo changes;

        int id = playersArray.at(i).toObject().value("id").toInt();
        QString direction = playersArray.at(i).toObject().value("direction").toString(); //варианты: up,down,left,right
        QJsonObject location = playersArray.at(i).toObject().value("location").toObject();
        int x = location.value("x").toInt();
        int y = location.value("y").toInt();
        bool isDead = playersArray.at(i).toObject().value("isDead").toBool();

        QList<Cell> tailChanges;
        QJsonArray trailChanges = playersArray.at(i).toObject().value("trailChanges").toArray();
        for (int j = 0; j < trailChanges.count(); j++) {
            int t_x = trailChanges.at(j).toObject().value("x").toInt();
            int t_y = trailChanges.at(j).toObject().value("y").toInt();
            QString t_type = trailChanges.at(j).toObject().value("type").toString(); //варианты: empty,trail
            if (t_type == "empty")
                tailChanges.push_back(Cell(Position(t_x, t_y), Empty));
            if (t_type == "trail")
                tailChanges.push_back(Cell(Position(t_x, t_y), Tail));
        }
        changes = PlayerInfo(id, "name", Position(x,y), direction, "color", isDead, tailChanges);
        playersChanges.push_back(changes);
    }
    Step(playersChanges);
    NextTurn(playersChanges);

}

/* Обработка сообщения от сервера типа "playerJoined
 * -------------------------------------------------
 * Парсим полученное сообщение, после чего
 * заполняем полученными данными информацию о новом игроке
 * и добавляем этого игрока в список модели.
*/
void ClientGameModel::MessagePlayerJoined(QJsonObject message)
{
    PlayerInfo playerToAdd;

    QJsonObject joinedPlayer = message.value("player").toObject();
    int id = joinedPlayer.value("id").toInt();
    QString name = joinedPlayer.value("name").toString();
    QString direction = joinedPlayer.value("direction").toString();
    QJsonObject location = joinedPlayer.value("location").toObject();
    int x = location.value("x").toInt();
    int y = location.value("y").toInt();
    QString color = joinedPlayer.value("color").toString();
    bool isDead = joinedPlayer.value("isDead").toBool();
    QJsonArray trail = joinedPlayer.value("trail").toArray();

    QList<Cell> tail;
    for (int i = 0; i < trail.count(); i++) {
        int t_x = trail.at(i).toObject().value("x").toInt();
        int t_y = trail.at(i).toObject().value("y").toInt();
        tail.push_back(Cell(Position(t_x, t_y), Tail));
    }

    gameLog.AboutPlayerConnection(name + "#" + QString::number(id), true);

    playerToAdd = PlayerInfo(id, name, Position(x, y), direction, color, isDead, tail);
}

void ClientGameModel::ConnectSlot(QString address, int port)
{
    this->address = address;
    this->port = port;
    ConnectSignal(address, port);
}

void ClientGameModel::DisconnectSlot()
{
    DisconnectSignal();
}

void ClientGameModel::PlayerInfoSlot(QString name, QString color, bool isPlayer)
{
    if (isPlayer)
        PlayerJoinSignal(name, color);
    else
        SpectatorSignal();
}

void ClientGameModel::GameStateSlot()
{
    if (gameState == "pause")
        GameStateSignal("running");
    if (gameState == "running")
        GameStateSignal("pause");
}

void ClientGameModel::StatusSlot()
{
    StatusSignal();
}

/* Обработка нажатия клавиш передвижения
 * -------------------------------------
 * В зависимости от нажатой клавиши отправляем сигнал на смену
 * движения. В свою очередь сигнал передаётся серверу через
 * tcpclient.
*/
void ClientGameModel::ChangeDirectionSlot(int key)
{
    if (isPlayer) {
        if (key == Qt::Key_W) {
            ChangeDirectionSignal("up");
        }
        if (key == Qt::Key_A) {
            ChangeDirectionSignal("left");
        }
        if (key == Qt::Key_S) {
            ChangeDirectionSignal("down");
        }
        if (key == Qt::Key_D) {
            ChangeDirectionSignal("right");
        }
    }
}

