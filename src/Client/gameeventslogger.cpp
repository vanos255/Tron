#include "gameeventslogger.h"

#include <QDateTime>
GameEventsLogger::GameEventsLogger(QObject *parent) : QObject(parent)
{
    maxLogSize = 20;
}

void GameEventsLogger::AboutConnection(bool connected, QString address, int port)
{
    QDateTime dt = QDateTime::currentDateTime();
    QString time = QString::number(dt.time().hour()) + ":" + QString::number(dt.time().minute()) + ":" + QString::number(dt.time().second()) + ">";

    if (connected)
        log.push_back(time + "Connected to " + address + ":" + QString::number(port));
    else
        log.push_back(time + "Disconnected from " + address + ":" + QString::number(port));

    LogReduction();
}

void GameEventsLogger::AboutPlayerConnection(QString name, bool connected)
{
    QDateTime dt = QDateTime::currentDateTime();
    QString time = QString::number(dt.time().hour()) + ":" + QString::number(dt.time().minute()) + ":" + QString::number(dt.time().second()) + ">";

    if (connected)
        log.push_back(time + name + " connected");
    else
        log.push_back(time + name + " disconnected");

    LogReduction();
}

void GameEventsLogger::AboutGameState(QString gState)
{
    QDateTime dt = QDateTime::currentDateTime();
    QString time = QString::number(dt.time().hour()) + ":" + QString::number(dt.time().minute()) + ":" + QString::number(dt.time().second()) + ">";

    if (gState == "lobby") {
        log.push_back(time + "LOBBY");
    }
    if (gState == "running") {
        log.push_back(time + "RUNNING");
    }
    if (gState == "pause") {
        log.push_back(time + "PAUSE");
    }
    if (gState == "gameover") {
        log.push_back(time + "GAME OVER");
    }

    LogReduction();
}

void GameEventsLogger::AboutDeadPlayers(QString name)
{
    QDateTime dt = QDateTime::currentDateTime();
    QString time = QString::number(dt.time().hour()) + ":" + QString::number(dt.time().minute()) + ":" + QString::number(dt.time().second()) + ">";

    log.push_back(time + name + " lost");

    LogReduction();
}

void GameEventsLogger::AboutWinner(QString name)
{
    QDateTime dt = QDateTime::currentDateTime();
    QString time = QString::number(dt.time().hour()) + ":" + QString::number(dt.time().minute()) + ":" + QString::number(dt.time().second()) + ">";

    log.push_back(time + "Winner is " + name);

    LogReduction();
}


QStringList GameEventsLogger::GetLogInfo()
{
    return log;
}

void GameEventsLogger::LogReduction()
{
    if (log.size() > maxLogSize)
        log.removeFirst();
}
