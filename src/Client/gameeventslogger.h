#ifndef GAMEEVENTSLOGGER_H
#define GAMEEVENTSLOGGER_H

#include <QObject>

class GameEventsLogger : public QObject
{
    Q_OBJECT
public:
    explicit GameEventsLogger(QObject *parent = 0);

    void AboutConnection(bool connected, QString address, int port);
    void AboutPlayerConnection(QString name, bool connected);
    void AboutGameState(QString gStatus);
    void AboutDeadPlayers(QString name);
    void AboutWinner(QString name);

    QStringList GetLogInfo();

private:
    int maxLogSize;
    QStringList log;

    void LogReduction();

signals:


public slots:

};

#endif // GAMEEVENTSLOGGER_H
