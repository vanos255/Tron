#ifndef LOCALSERVER_H
#define LOCALSERVER_H

#include <QObject>
#include <QProcess>
#include <QDir>
#include <QThread>

#include "./Dialogs/startserverdialog.h"

class LocalServer : public QObject
{
    Q_OBJECT
public:
    explicit LocalServer(QObject *parent = 0);

    ~LocalServer();

    int StartServer();
    void StopServer();

private:
    QProcess *server;

    bool isStarted;

    int port;
    int rows;
    int columns;
    int players;
    int bots;
    int timer;

    void CheckSetup(int port, int rows, int columns, int players, int bots, int timer);

    StartServerDialog *setup;

signals:

public slots:
    void GetSetup(int port, int rows, int columns, int players, int bots, int timer);

};

#endif // LOCALSERVER_H
