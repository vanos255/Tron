#ifndef CLIENTGAMEMODEL_H
#define CLIENTGAMEMODEL_H

#include "structures.h"
#include "gameeventslogger.h"

#include <QObject>

#include <QList>

#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>

// удалить. нужен для qDebug
#include <QtGui>



class ClientGameModel : public QObject
{
    Q_OBJECT
public:

    ClientGameModel(QObject *parent = 0);

    ~ClientGameModel();

    void SetMyID(int id);
    int GetMyID();

    void SetIsPlayer(bool isPlayer);
    bool GetIsPlayer();

    void SetGameState(QString gameState);
    QString GetGameState();

    void SetPlayersInfo(QList<PlayerInfo> players);
    QList<PlayerInfo> GetPlayersInfo();

    void AddPlayer(PlayerInfo player);
    void RemovePlayer(int id);

    void Initialization(int myID, bool isPlayer, QString gameState, Position mapSize, QList<PlayerInfo> players);
    void Step(QList<PlayerInfo> changes);

private:
    bool isConnected;
    QString address;
    int port;

    int maxPlayers;

    int myId;
    bool isPlayer;
    QString gameState;
    Position mapSize;
    QList<PlayerInfo> players;

    // Логирование игровых событий
    GameEventsLogger gameLog;

    void FindWinner();
    // Помощь парсеру. Обработка больших сообщений.
    void MessageStatus(QJsonObject message);
    void MessageNextTurn(QJsonObject message);
    void MessagePlayerJoined(QJsonObject message);

signals:
    // Отправляемые серверу сигналы
    void ConnectSignal(QString address, int port);
    void DisconnectSignal();
    void PlayerJoinSignal(QString name, QString color);
    void SpectatorSignal();
    void GameStateSignal(QString gameState);
    void StatusSignal();
    void ChangeDirectionSignal(QString direction);

    // Отправка изменений в логе
    void FillLogLIst(QStringList log);

    // Отправляемые представлению сигналы
    void Initialised(Position mapSize, QList<PlayerInfo> players, int myID, int maxPlayers);
    void NextTurn(QList<PlayerInfo> playersChanges);

    void PlayerJoined(int id);
    void PlayerLeft(int id);
    void GameStateChanged(QString newState);

    void RoleConfirmation(bool isPlayer);
    void ConnectionStatus(bool isConnected);

public slots:
    // Получаемые от представления сигналы
    void ConnectSlot(QString address, int port);
    void DisconnectSlot();
    void PlayerInfoSlot(QString name, QString color, bool isPlayer);
    void GameStateSlot();
    void StatusSlot();
    void ChangeDirectionSlot(int key);

    // Получаемые от сервера сигналы
    void ParseMessageSlot(QJsonObject initializationInfo);

    void ConnectedStatusSlot();
    void DisconnectedStatusSlot();

};

#endif // CLIENTGAMEMODEL_H
