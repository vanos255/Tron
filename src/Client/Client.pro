#-------------------------------------------------
#
# Project created by QtCreator 2017-04-04T13:27:11
#
#-------------------------------------------------

QT += core gui
QT += core network
CONFIG +=warn_off

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Client
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include( ../../common.pri )
include( ../../app.pri )

SOURCES += main.cpp\
    mainwindow.cpp \
    clientgamemodel.cpp \
    gamearea.cpp \
    gameeventslogger.cpp \
    localserver.cpp \
    ./Dialogs/chooseserverdialog.cpp \
    ./Dialogs/playerinfodialog.cpp \
    ./Dialogs/startserverdialog.cpp \
    ../libtcpclient/tcpclient.cpp \

HEADERS  += mainwindow.h \
    structures.h \
    clientgamemodel.h \
    gamearea.h \
    gameeventslogger.h \
    localserver.h \
    ./Dialogs/chooseserverdialog.h \
    ./Dialogs/playerinfodialog.h \
    ./Dialogs/startserverdialog.h \
    ../libtcpclient/tcpclient.h \

FORMS    += mainwindow.ui \
    ./Dialogs/chooseserverdialog.ui \
    ./Dialogs/playerinfodialog.ui \
    ./Dialogs/startserverdialog.ui
