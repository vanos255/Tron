#ifndef CHOOSESERVERDIALOG_H
#define CHOOSESERVERDIALOG_H

#include <QDialog>

namespace Ui {
class ChooseServerDialog;
}

class ChooseServerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChooseServerDialog(QWidget *parent = 0);
    ~ChooseServerDialog();

private:
    Ui::ChooseServerDialog *ui;

signals:
    void GetServerInfo(QString address, int port);
    void StartServer();
    void StopServer();

public slots:
    void ConnectToLocalServer(QString address, int port);

private slots:
    void on_buttonBox_accepted();
    void on_startServer_clicked();
    void on_stopServer_clicked();
};

#endif // CHOOSESERVERDIALOG_H
