#include "chooseserverdialog.h"
#include "ui_chooseserverdialog.h"

#include "startserverdialog.h"

ChooseServerDialog::ChooseServerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChooseServerDialog)
{
    ui->setupUi(this);

    ui->ip->setText("127.0.0.1");
    ui->port->setText("3000");
}

ChooseServerDialog::~ChooseServerDialog()
{
    delete ui;
}

void ChooseServerDialog::on_buttonBox_accepted()
{
    GetServerInfo(ui->ip->text(), ui->port->text().toInt());
}

void ChooseServerDialog::on_startServer_clicked()
{
    StartServer();
}

void ChooseServerDialog::on_stopServer_clicked()
{
    StopServer();
}

void ChooseServerDialog::ConnectToLocalServer(QString address, int port)
{
    ui->ip->setText(address);
    ui->port->setText(QString::number(port));
    ui->buttonBox->accepted();
}
