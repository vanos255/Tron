#ifndef PLAYERINFODIALOG_H
#define PLAYERINFODIALOG_H

#include <QDialog>

namespace Ui {
class PlayerInfoDialog;
}

class PlayerInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PlayerInfoDialog(QWidget *parent = 0);
    ~PlayerInfoDialog();

private:
    Ui::PlayerInfoDialog *ui;

    QString name;
    QString color;
    bool isPlayer;

signals:
    void GetPlayerInfo(QString name, QString color, bool isPlayer);
private slots:
    void on_buttonBox_accepted();
    void on_colorButton_clicked();
};

#endif // PLAYERINFODIALOG_H
