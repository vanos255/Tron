#include "startserverdialog.h"
#include "ui_startserverdialog.h"

StartServerDialog::StartServerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StartServerDialog)
{
    ui->setupUi(this);

    ui->portLine->setText("3000");
    ui->rowsLine->setText("10");
    ui->columnsLine->setText("10");
    ui->playersLine->setText("2");
    ui->botsLine->setText("0");
    ui->timerLine->setText("300");
}

StartServerDialog::~StartServerDialog()
{
    delete ui;
}

void StartServerDialog::on_buttonBox_accepted()
{
    SetSetup(ui->portLine->text().toInt(), ui->rowsLine->text().toInt(), ui->columnsLine->text().toInt(), ui->playersLine->text().toInt(), ui->botsLine->text().toInt(), ui->timerLine->text().toInt());
}
