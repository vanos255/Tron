#ifndef STARTSERVERDIALOG_H
#define STARTSERVERDIALOG_H

#include <QDialog>

namespace Ui {
class StartServerDialog;
}

class StartServerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StartServerDialog(QWidget *parent = 0);
    ~StartServerDialog();

private:
    Ui::StartServerDialog *ui;

signals:
    void SetSetup(int port, int rows, int columns, int players, int bots, int timer);

private slots:
    void on_buttonBox_accepted();
};

#endif // STARTSERVERDIALOG_H
