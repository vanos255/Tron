#include "playerinfodialog.h"
#include "ui_playerinfodialog.h"

#include <QColorDialog>
#include <QMessageBox>

PlayerInfoDialog::PlayerInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlayerInfoDialog)
{
    ui->setupUi(this);
}

PlayerInfoDialog::~PlayerInfoDialog()
{
    delete ui;
}

void PlayerInfoDialog::on_buttonBox_accepted()
{
    this->name = ui->name_line->text();

    if (ui->sliderPlayerSpectator->value() == 0)
        isPlayer = true;
    else if (ui->sliderPlayerSpectator->value() == 1)
        isPlayer = false;

    if (!name.isEmpty())
        GetPlayerInfo(name, color , isPlayer);
    else {
        QMessageBox::critical(this, "Error!", "Name is empty!\nYou'll be Spectator!");
    }
}

void PlayerInfoDialog::on_colorButton_clicked()
{
    QColor color = QColorDialog::getColor(Qt::blue);
    if (color.isValid() ) {
        this->color = color.name();
        ui->colorButton->setStyleSheet(" background-color: " + color.name());
    } else {
        this->color = "#000000";
        ui->colorButton->setStyleSheet(" background-color: #000000");
    }
}
