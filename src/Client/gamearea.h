#ifndef GAMEAREA_H
#define GAMEAREA_H

#include <QWidget>

#include "structures.h"

class GameArea : public QWidget
{
    Q_OBJECT
public:
    explicit GameArea(QWidget *parent = 0);

    void Initialization(Position mapSize, QList<DrawingInfo> toDraw);

    // Работа с отрисовкой игрового поля
    void Redraw(QList<DrawingInfo> changes);
    void DrawLobbyInfo(QList<PlayerInfo> playersList, int maxPlayers);

private:
    int cellsize;
    Position mapSize;
    QList<DrawingInfo> toDraw;

    bool isLobbyInfoDrawing;

    QList<PlayerInfo> playersList;
    int maxPlayers;

    // Подкоманды отрисовки
    void CreateGrid();
    void DrawCells();
    void DrawLobby();

protected:
    void paintEvent(QPaintEvent *);

signals:

public slots:
};

#endif // GAMEAREA_H
