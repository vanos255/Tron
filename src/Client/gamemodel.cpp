#include "gamemodel.h"

GameModel::GameModel(QObject *parent) : QObject(parent)
{
    mapSize = Position(5,5);
    map = new Cell* [mapSize.y];
    for (int i = 0; i < mapSize.y; i++)
        map[i] = new Cell [mapSize.x];

    for (int i = 0; i < mapSize.y; i++) {
        for (int j = 0; j < mapSize.x; j++) {
            map[i][j] = Cell(Position(j,i), Empty);
        }
    }
}

GameModel::GameModel(QList<PlayerInfo> players, Position mapSize)
{
    map = new Cell* [mapSize.y];
    for (int i = 0; i < mapSize.y; i++)
        map[i] = new Cell [mapSize.x];

    for (int i = 0; i < mapSize.y; i++) {
        for (int j = 0; j < mapSize.x; j++) {
            map[i][j] = Cell(Position(j,i), Empty);
        }
    }

    this->mapSize = mapSize;
    SetPlayersInfo(players);
}

GameModel::~GameModel()
{
    /*
    for (int i = 0; i < mapSize; i++) {
        if (map[i])
            delete [] map[i];
    }
    delete [] map;
    */
}

void GameModel::SetMyID(int id)
{
    myId = id;
}

int GameModel::GetMyID()
{
    return myId;
}

void GameModel::SetIsPlayer(bool isPlayer)
{
    this->isPlayer = isPlayer;
}

bool GameModel::GetIsPlayer()
{
    return isPlayer;
}

void GameModel::SetGameState(QString gameState)
{
    this->gameState = gameState;
}

QString GameModel::GetGameState()
{
    return gameState;
}

void GameModel::SetMapChanges(QList<Cell> changes)
{

}

QList<Cell> GameModel::GetMapChanges()
{
    QList<Cell> m;
    return m;
}

void GameModel::SetPlayersInfo(QList<PlayerInfo> players)
{
    this->players = players;
}

QList<PlayerInfo> GameModel::GetPlayersInfo()
{
    QList<PlayerInfo> playersInfo = players;
    return playersInfo;
}

void GameModel::AddPlayer(PlayerInfo player)
{
    players.push_back(player);
}

void GameModel::RemovePlayer(int id)
{
    for (int i = 0; i < players.count(); i++) {
        if (players[i].id = id) {
            players.removeAt(i);
            break;
        }
    }
}

void GameModel::Initialization(int myID, bool isPlayer, QString gameState, Position mapSize, QList<PlayerInfo> players)
{
    this->myId = myID;
    this->isPlayer = isPlayer;
    this->gameState = gameState;
    this->mapSize = mapSize;
    SetPlayersInfo(players);
}

void GameModel::Step(QList<PlayerInfo> changes)
{
    QList<PlayerInfo> currentInfo = GetPlayersInfo();

    QList<Cell> mapChanges;

    for (int i = 0; i < currentInfo.count(); i++) {
        for (int j = 0; j < changes.count(); j++) {
            if (currentInfo[i].id == changes[j].id) {
                currentInfo[i].position = changes[j].position;
                currentInfo[i].direction = changes[j].direction;
                currentInfo[i].isDead = changes[j].isDead;
                currentInfo[i].tail.append(changes[j].tail);

                if (!currentInfo[i].isDead)
                    mapChanges.push_back(Cell(changes[j].position, Player));
                mapChanges.append(changes[i].tail);
            }
        }
    }
    foreach (Cell c, mapChanges) {
        map[c.position.y][c.position.x].state = c.state;
    }
}

/* Инициализация при получении JSON пакета.
 * ----------------------------------------
 * Из пакета получаем данные.
 * Парсим документ в зависимости от типа сообщения.
 *
*/
void GameModel::ParseMessageSlot(QJsonObject message)
{
    QString messageType = "";
    messageType = message.value("messageType").toString();

    if (messageType == "status")
        MessageStatus(message);

    if (messageType == "nextTurn")
        MessageNextTurn(message);

    if (messageType == "playerJoined")
        MessagePlayerJoined(message);

    if (messageType == "playerLeaved") {
        qDebug() << "ИГРОК ПОКИНУЛ ИГРУ";
        int leavedPlayerID = message.value("player").toObject().value("id").toInt();
        RemovePlayer(leavedPlayerID);
    }
    if (messageType == "joinAcknowledge") {
        qDebug() << "СЕРВЕР ЗНАЕТ ОБО МНЕ";
        bool isPlayer = true;
        int myID = message.value("client").toObject().value("playerId").toInt();

        SetIsPlayer(isPlayer);
        SetMyID(myID);
    }
    if (messageType == "spectateAcknowledge")
    {
        qDebug() << "СЕРЕВЕР ЗНАЕТ, ЧТО Я ЗРИТЕЛЬ";
        bool isPlayer = false;
        SetIsPlayer(isPlayer);
    }
    if (messageType == "gameStateChanged")
    {
        qDebug() << "СОСТОЯНИЕ ИГРЫ ИЗМЕНЕНО";
        QString gState = message.value("gameState").toString(); //варианты: lobby,running,pause,gameover
        SetGameState(gState);
    }

}

/* Обрабоска сообщения от сервера типа "status"
 * --------------------------------------------
 * Парсим полученные данные о карте, игроках, состоянии игры и роли клиента
*/
void GameModel::MessageStatus(QJsonObject message)
{
    qDebug() << "ПРИНЯТ СТАТУС";


    QJsonObject client = message.value("client").toObject();
    bool isPlayer = client.value("isPlayer").toBool();
    int playerId = client.value("playerId").toInt();

    QString state = message.value("gameState").toString();

    QList<PlayerInfo> players;
    QJsonArray playersArray = message.value("players").toArray();
    for (int i = 0; i < playersArray.count(); i++) {
        PlayerInfo player;

        int id = playersArray.at(i).toObject().value("id").toInt();
        QString name = playersArray.at(i).toObject().value("name").toString();
        QString direction = playersArray.at(i).toObject().value("direction").toString();
        QJsonObject location = playersArray.at(i).toObject().value("location").toObject();
        int x = location.value("x").toInt();
        int y = location.value("y").toInt();
        QString color = playersArray.at(i).toObject().value("color").toString();
        bool isDead = playersArray.at(i).toObject().value("isDead").toBool();

        QList<Cell> tail;
        QJsonArray trail = playersArray.at(i).toObject().value("trail").toArray();
        for (int j = 0; j < trail.count(); j++) {
            int t_x = trail.at(j).toObject().value("x").toInt();
            int t_y = trail.at(j).toObject().value("y").toInt();

            tail.push_back(Cell(Position(t_x, t_y), Tail));
        }
        player = PlayerInfo(id, name, Position(x, y), direction, color, isDead, tail);
        players.push_back(player);
    }

    QJsonObject map = message.value("map").toObject();
    int rows = map.value("rows").toInt();
    int columns = map.value("columns").toInt();

    // Инициализируем модель
    Initialization(playerId, isPlayer, state, Position(rows, columns), players);
}

/* Обработка сообщения от сервера типа "nextTurn"
 * ----------------------------------------------
 * Парсим полученную информацию о новой позиции игроков,
 * изменении в их хвостах и в игре ли игрок.
*/
void GameModel::MessageNextTurn(QJsonObject message)
{
    qDebug() << "ПРИНЯТ СЛЕДУЮЩИЙ ХОД";

    QList<PlayerInfo> playersChanges;

    QJsonArray playersArray = message.value("players").toArray();
    for (int i = 0; i < playersArray.count(); i++) {
        PlayerInfo changes;

        int id = playersArray.at(i).toObject().value("id").toInt();
        QString direction = playersArray.at(i).toObject().value("direction").toString(); //варианты: up,down,left,right
        QJsonObject location = playersArray.at(i).toObject().value("location").toObject();
        int x = location.value("x").toInt();
        int y = location.value("y").toInt();
        bool isDead = playersArray.at(i).toObject().value("isDead").toBool();

        QList<Cell> tailChanges;
        QJsonArray trailChanges = playersArray.at(i).toObject().value("trailChanges").toArray();
        for (int j = 0; j < trailChanges.count(); j++) {
            int t_x = trailChanges.at(j).toObject().value("x").toInt();
            int t_y = trailChanges.at(j).toObject().value("y").toInt();
            QString t_type = trailChanges.at(j).toObject().value("type").toString(); //варианты: empty,trail
            if (t_type == "empty")
                tailChanges.push_back(Cell(Position(t_x, t_y), Empty));
            if (t_type == "trail")
                tailChanges.push_back(Cell(Position(t_x, t_y), Tail));
        }
        changes = PlayerInfo(id, "name", Position(x,y), direction, "color", isDead, tailChanges);
        playersChanges.push_back(changes);
    }

    Step(playersChanges);

}

/* Обработка сообщения от сервера типа "playerJoined
 * -------------------------------------------------
 * Парсим полученное сообщение, после чего
 * заполняем полученными данными информацию о новом игроке
 * и добавляем этого игрока в список модели.
*/
void GameModel::MessagePlayerJoined(QJsonObject message)
{
    qDebug() << "ИГРОК ПРИСОЕДИНИЛСЯ";

    PlayerInfo playerToAdd;

    QJsonObject joinedPlayer = message.value("player").toObject();
    int id = joinedPlayer.value("id").toInt();
    QString name = joinedPlayer.value("name").toString();
    QString direction = joinedPlayer.value("direction").toString();
    QJsonObject location = joinedPlayer.value("location").toObject();
    int x = location.value("x").toInt();
    int y = location.value("y").toInt();
    QString color = joinedPlayer.value("color").toString();
    bool isDead = joinedPlayer.value("isDead").toBool();
    QJsonArray trail = joinedPlayer.value("trail").toArray();

    QList<Cell> tail;
    for (int i = 0; i < trail.count(); i++) {
        int t_x = trail.at(i).toObject().value("x").toInt();
        int t_y = trail.at(i).toObject().value("y").toInt();
        tail.push_back(Cell(Position(t_x, t_y), Tail));
    }

    playerToAdd = PlayerInfo(id, name, Position(x, y), direction, color, isDead, tail);

}
