#include "gamearea.h"

#include <QPainter>

GameArea::GameArea(QWidget *parent) : QWidget(parent)
{
    cellsize = 10;
    mapSize = Position(6,8);
    isLobbyInfoDrawing = false;
}

void GameArea::Initialization(Position mapSize, QList<DrawingInfo> toDraw)
{
    this->mapSize = mapSize;
    this->toDraw = toDraw;
}

// Работа с отрисовкой игрового поля
void GameArea::Redraw(QList<DrawingInfo> changes)
{
    isLobbyInfoDrawing = false;
    this->toDraw = changes;
}

void GameArea::DrawLobbyInfo(QList<PlayerInfo> playersList, int maxPlayers)
{
    isLobbyInfoDrawing = true;
    this->playersList = playersList;
    this->maxPlayers = maxPlayers;
}

// Подкоманды отрисовки
void GameArea::CreateGrid()
{
    QPainter p(this);
    p.begin(this);
    p.setPen(Qt::gray);

    for (int i = 0; i < mapSize.x + 1; i++) {
        p.drawLine(i * cellsize, 0, i * cellsize, mapSize.y * cellsize);
    }
    for (int i = 0; i < mapSize.y + 1; i++) {
        p.drawLine(0,i * cellsize, mapSize.x * cellsize, i * cellsize);
    }
}

void GameArea::DrawCells()
{
    QPainter p(this);
    p.begin(this);

    for (int i = 0; i < toDraw.count(); i++) {
        p.setPen(toDraw[i].cellColor);
        p.setBrush(QBrush(toDraw[i].cellColor, Qt::SolidPattern));
        int rectSize;
        if (toDraw[i].cell.state == Player)
            rectSize = 8;
        else
            if (toDraw[i].cell.state == Tail)
                rectSize = 6;
        // Централизуем ячейки относительно клеток
        int drawPosY = (mapSize.y - 1 - toDraw[i].cell.position.y) * cellsize + ( (cellsize - rectSize) / 2 );
        int drawPosX = toDraw[i].cell.position.x * cellsize + ( (cellsize - rectSize) / 2 );

        p.drawRect(drawPosX, drawPosY, rectSize, rectSize);
    }
}

void GameArea::DrawLobby()
{
    int lineHeight = 16;
    QPainter p(this);
    p.begin(this);

    QFont font("Arial", 12);
    font.setBold(true);

    p.setPen(Qt::black);
    p.setFont(font);

    // Строка состояния
    QString toDraw = "Players connected (" + QString::number(playersList.count()) + "/" + QString::number(maxPlayers) + "):";

    p.drawText(this->width()/4, lineHeight, toDraw);

    // список игроков
    font.setBold(false);
    int ident = lineHeight + lineHeight;
    for (int i = 0; i < playersList.count(); i++) {
        p.setPen(playersList[i].color);
        toDraw = playersList[i].name + "#" + QString::number(playersList[i].id);

        p.drawText(this->width()/3, ident, toDraw);
        ident += lineHeight;
    }
}

void GameArea::paintEvent(QPaintEvent *)
{
    if (!isLobbyInfoDrawing) {
        CreateGrid();
        DrawCells();
    }
    else {
        DrawLobby();
    }
}
