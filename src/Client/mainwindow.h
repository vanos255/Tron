#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QList>

#include <QTcpSocket>
#include <QJsonObject>
#include <QKeyEvent>
#include <QPainter>

#include "structures.h"

#include "clientgamemodel.h"
#include "localserver.h"

// Диалоговые окна
#include "./Dialogs/chooseserverdialog.h"
#include "./Dialogs/playerinfodialog.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();
protected:

    virtual void keyPressEvent(QKeyEvent *event);
    void paintEvent(QPaintEvent *event);


private:

    Ui::MainWindow *ui;

    // Диалоговые окна
    PlayerInfoDialog *pid;
    ChooseServerDialog *csd;

    // Запущен ли сервер на клиенте
    bool serverStarted;
    LocalServer localServer;


    // Информация о конфигурации сервера, к которому подключились
    QString serverAddress;
    int serverPort;
    int maxPlayers;

    // Игровая информация
    int myID;
    QString gameState;
    QString playerName;
    QString playerColor;
    bool isPlayer;
    bool isConnectedToServer;

    int cellsize;
    Position mapSize;
    QList<PlayerInfo> players;

    // Работа с отрисовкой окна
    void Redraw();

    // Подготовка информации к отрисовке
    QList<DrawingInfo> DrawInfoPreparing();
    void UpdateGameInfo();

    // Автоопределение размера виджета с игровым полем
    void GameAreaAutosize();

    // Контроль доступности кнопок
    void ButtonsAvailability();

signals:
    // Отправляемые для модели сигналы
    void ConnectSignal(QString address, int port);
    void DisconnectSignal();
    void PlayerInfoSignal(QString name, QString color, bool isPlayer);
    void GameStateSignal();
    void StatusSignal();
    void ChangeDirectionSignal(int key);

    // Подключение к локальному серверу
    void ConnectToLocalServer(QString,int);


public slots:
    // Получаемые от модели сигналы
    void Initialised(Position mapSize, QList<PlayerInfo> players, int myID, int maxPlayers);
    void NextTurn(QList<PlayerInfo> playersChanges);

    // Обработка игровых событий
    void GameSrtateChanged(QString newState);
    void RoleConfirmation(bool isPlayer);
    void ConnectionStatus(bool isConnected);

    // Отрисовка лога игровых событий
    void FillLogList(QStringList log);

    // Получаемые от дочерних окон сигналы
    void GetServerInfo(QString address, int port);
    void GetPlayerInfo(QString name, QString color, bool isPlayer);

    // Работа с локальным сервером
    void LocalServerStart();
    void LocalServerStop();

private slots:
    // Обработка нажатия кнопок на форме
    void on_pause_clicked();
    void on_disconnectButton_clicked();
    void on_changePlayerInfo_clicked();
    void on_changeServer_clicked();

};

#endif // MAINWINDOW_H
