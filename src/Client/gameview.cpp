#include "gameview.h"
#include "ui_gameview.h"

#include <QPainter>
#include <QList>

GameView::GameView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GameView)
{
    ui->setupUi(this);

    InitializeMap();

}

GameView::~GameView()
{
    delete ui;
}

void GameView::CreateGrid(int size)
{
    QPainter p(this);
    p.begin(this);
    p.setPen(Qt::gray);
    for (int i = 0; i < size + 1; i++){
        p.drawLine(i * cellsize, 0, i * cellsize, size * cellsize);
        p.drawLine(0,i * cellsize, size * cellsize, i * cellsize);
    }
}

void GameView::DrawCells()
{
    QList<Position> players = gamesState.GetPlayersPositions();

    QPainter p(this);
    p.begin(this);
    p.setPen(Qt::red);

    // Отрисоква игроков
    for (int i = 0; i < players.count(); i++){
        p.drawRect(players[i].x * cellsize, players[i].y * cellsize, 8, 8);
    }

    // Отрисовка хвостов
    p.setPen(Qt::darkRed);
    for (int i = 0; i < mapSize; i++)
        for (int j = 0; j < mapSize; j++)
        {
            if (gamesState.map[i][j].state == Tail)
                p.drawRect(i * cellsize, j * cellsize, 5, 5);
        }

}

void GameView::InitializeMap()
{
    QList<Position> players;
    players.push_back(Position(3,3));
    players.push_back(Position(4,15));
    gamesState.SetPlayersPositions(players);

    gamesState = ClientGameState(mapSize, players);
}

void GameView::paintEvent(QPaintEvent *)
{
    Redraw();
}

// Эмуляция сообщения о шаге от сервера
void GameView::on_pushButton_3_clicked()
{
    QList<Position> players = gamesState.GetPlayersPositions();

    // Добавление изменений
    QList<Cell> changes;
    Cell ch = Cell(Position(players[0].x, players[0].y), Tail);
    changes.push_back(ch);

    ch = Cell(Position(players[1].x, players[1].y), Tail);
    changes.push_back(ch);
    gamesState.SetMapChanges(changes);


    // Добавление новых позиций игроков
    players[0] = Position(players[0].x, players[0].y + 1);
    players[1] = Position(players[1].x + 1, players[1].y);

    gamesState.Step(players, changes);
    Redraw();

    repaint();
}

void GameView::Redraw()
{
    CreateGrid(mapSize);
    DrawCells();
}
