#include "localserver.h"

#include <QTcpSocket>
#include <QMessageBox>

LocalServer::LocalServer(QObject *parent) : QObject(parent)
{
    isStarted = false;

    server = nullptr;

    port = 0;
    rows = 20;
    columns = 20;
    players = 4;
    bots = 0;

    setup = new StartServerDialog();

    QObject::connect(setup, SIGNAL(SetSetup(int,int,int,int,int,int)), this, SLOT(GetSetup(int,int,int,int,int,int)));
}

LocalServer::~LocalServer()
{
    setup->deleteLater();
    server->deleteLater();
}

int LocalServer::StartServer()
{
    setup->show();
    if (setup->exec() == QDialog::Accepted) {
        if (isStarted)
            return port;
    }
    return 0;
}

void LocalServer::StopServer()
{
    if (server){
        server->close();
        server=nullptr;
    }
}

void LocalServer::CheckSetup(int port, int rows, int columns, int players, int bots, int timer)
{
    this->port = port;
    this->rows = rows;
    this->columns = columns;
    this->players = players;
    this->bots = bots;
    this->timer = timer;

    if (port <= 0)
        this->port = 3000;

    if (rows < 10)
        this->rows = 10;
    if (rows > 100)
        this->rows = 100;

    if (columns < 10)
        this->columns = 10;
    if (columns > 140)
        this->columns = 140;

    if (players < 2)
        this->players = 2;
    if (players > 16)
        this->players = 16;

    if (bots < 0)
        this->bots = 0;
    if (bots > players)
        this->bots = players;

    if (timer<0)
        this->timer=0;
}

void LocalServer::GetSetup(int port, int rows, int columns, int players, int bots, int timer)
{
    CheckSetup(port, rows, columns, players, bots, timer);

    QTcpSocket *socket = new QTcpSocket(this);
    socket->connectToHost("127.0.0.1", this->port);
    if (socket->waitForConnected(1000)) {
        socket->close();
        isStarted = false;
        QMessageBox errorMessage;
        QString message = "Server starting failed! \n Port" + QString::number(this->port) + " is busy";
        errorMessage.setWindowTitle("Error!");
        errorMessage.setText(message);
        errorMessage.show();
        errorMessage.exec();
    }
    else {
        socket->close();
        socket->abort();
        QStringList arguments;
        arguments << "-o" << QString::number(this->port) << "-r" << QString::number(this->rows)
                  << "-c" << QString::number(this->columns) << "-p" << QString::number(this->players)
                  << "-t" << QString::number(this->timer);

        server = new QProcess(this);
        QString file = QDir::currentPath() + "/Server";
        server->start(file, arguments);
        QThread::sleep(1);
        // Проверяем, запустился ли сервер
        socket->connectToHost("127.0.0.1", this->port);
        if (socket->waitForConnected(2000)) {
            socket->close();
            isStarted = true;

            for (int i = 0; i < this->bots; i++) {
                QProcess *bot = new QProcess(this);
                QString botFile = QDir::currentPath() + "/SimpleBot";
                QStringList botArguments;
                botArguments << "127.0.0.1" << QString::number(this->port);
                bot->start(botFile,botArguments);
            }

            QMessageBox setupMessage;
            setupMessage.setWindowTitle("Server started!");
            QString message = "Server started with setups:";
            message += "\nPort: " + QString::number(this->port);
            message += "\nRows: "+ QString::number(this->rows) + " Columns: "+ QString::number(this->columns);
            message += "\nPlayers: " + QString::number(this->players);
            message += "\nBots: " + QString::number(this->bots);
            message += "\nTimer: " + QString::number(this->timer);
            setupMessage.setText(message);
            setupMessage.show();
            setupMessage.exec();
        }
        else {
            isStarted = false;
            QMessageBox errorMessage;
            QString message = "Server starting failed!";
            errorMessage.setWindowTitle("Error!");
            errorMessage.setText(message);
            errorMessage.show();
            errorMessage.exec();
        }
    }
    socket->deleteLater();
}
