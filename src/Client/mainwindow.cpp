#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Инициализация начальной информации
    cellsize = 10;
    gameState = "lobby";
    playerName = "Player Name";
    playerColor = "#000000";
    isPlayer = false;
    isConnectedToServer = false;

    mapSize = Position(5,6);

    pid = new PlayerInfoDialog();
    csd = new ChooseServerDialog();

    ButtonsAvailability();

    csd->show();

    QObject::connect(pid, SIGNAL(GetPlayerInfo(QString,QString,bool)), this, SLOT(GetPlayerInfo(QString,QString,bool)));
    QObject::connect(csd, SIGNAL(GetServerInfo(QString,int)), this, SLOT(GetServerInfo(QString,int)));

    // Local server
    QObject::connect(csd, SIGNAL(StartServer()), this, SLOT(LocalServerStart()));
    QObject::connect(csd, SIGNAL(StopServer()), this, SLOT(LocalServerStop()));
    // Connect to local server
    QObject::connect(this, SIGNAL(ConnectToLocalServer(QString,int)), csd, SLOT(ConnectToLocalServer(QString,int)));

}

MainWindow::~MainWindow()
{
    pid->deleteLater();
    csd->deleteLater();

    localServer.StopServer();
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    int key = event->key();
    ChangeDirectionSignal(key);
}

// Работа с отрисовкой окна
void MainWindow::paintEvent(QPaintEvent *)
{
    Redraw();
}

void MainWindow::Redraw()
{
    if (gameState == "lobby")
        ui->gameAreaWidget->DrawLobbyInfo(players, maxPlayers);
    else {
        QList<DrawingInfo> changes = DrawInfoPreparing();
        ui->gameAreaWidget->Redraw(changes);
    }
    UpdateGameInfo();
}


/* Подготовка имеющейся информации для отрисовки
 * ---------------------------------------------
 * Собираем информацию о состоянии положений игроков и их хвостах.
 * Используется для инициализации информации для отрисовки,
 * а так же для отрисовки изменений.
*/
QList<DrawingInfo> MainWindow::DrawInfoPreparing()
{
    QList<DrawingInfo> changes;

    for (int i = 0; i < players.count(); i++) {
        if (players[i].isDead)
            continue;

        // Добавить позицию игрока
        DrawingInfo cellToDraw;
        cellToDraw.cell.position = players[i].position;
        cellToDraw.cell.state = Player;
        cellToDraw.cellColor = players[i].color;

        changes.push_back(cellToDraw);

        // Добавить хвосты игрока
        for (int j = 0; j < players[i].tail.count(); j++) {
            cellToDraw.cell.position = players[i].tail[j].position;
            cellToDraw.cell.state = Tail;
            changes.push_back(cellToDraw);
        }
    }

    return changes;
}

void MainWindow::UpdateGameInfo()
{
    ui->gameState->setText(this->gameState);
    if (isPlayer)
        ui->playerName->setText(this->playerName + "#" + QString::number(this->myID));
    else {
        ui->playerName->setText(this->playerName);
        this->playerColor = "#000000";
    }

    ui->playerName->setStyleSheet("color: " + this->playerColor);

    if (isConnectedToServer) {
        ui->connectedServer->setText(serverAddress);
        ui->connectedPort->setText(QString::number(serverPort));
        ui->connectedServer->setStyleSheet("color: green");
        ui->connectedPort->setStyleSheet("color: green");
    } else {
        ui->connectedServer->setText("Disconnected");
        ui->connectedPort->setText("Disconnected");
        ui->connectedServer->setStyleSheet("color: red");
        ui->connectedPort->setStyleSheet("color: red");
    }

    if (isPlayer)
        ui->playerRole->setText("Player");
    else
        ui->playerRole->setText("Spectator");

    if (gameState == "lobby")
        ui->gameState->setText("Lobby");
    if (gameState == "running")
        ui->gameState->setText("Running");
    if (gameState == "pause")
        ui->gameState->setText("Pause");
    if (gameState == "gameover")
        ui->gameState->setText("Game Over!");
}

// Автоопределение размера виджета с игровым полем
void MainWindow::GameAreaAutosize()
{
    int x = mapSize.x * cellsize + cellsize;
    int y = mapSize.y * cellsize + cellsize;

    if (x < 300)
        x = 300;
    if (y < 300)
        y = 300;

    ui->gameAreaWidget->setMinimumSize(x,y);


    if (!this->isMaximized()) {
        this->setMinimumSize(this->minimumSize());
        int xPos = this->geometry().x();
        int yPos = this->geometry().y();
        this->setGeometry(xPos, yPos, this->minimumWidth(), this->minimumHeight());
    }
}

// Контроль доступности кнопок
void MainWindow::ButtonsAvailability()
{
    // Если не подключены к серверу - запрещаемы использовать кнопки изменения информации игрока, паузы и отключения от сервера.
    if (!isConnectedToServer) {
        ui->disconnectButton->setEnabled(false);
        ui->changePlayerInfo->setEnabled(false);
        ui->pause->setEnabled(false);
    }
    // Если подключены к серверу - разрешаем использование кнопки отключения от сервера
    else {
        ui->disconnectButton->setEnabled(true);
        // Если подключены и в лобби/конец игры - разрешаем изменять информацию об игроке, но запрещаем паузу
        if (gameState == "lobby" || gameState == "gameover") {
            ui->pause->setEnabled(false);
            ui->changePlayerInfo->setEnabled(true);
        }
        // Если подключены и в процессе игры или на паузе - если в роли игрока, разрешаем использовать паузу, иначе запрещаем
        else {
            if (gameState == "running" || gameState == "pause") {
                ui->changePlayerInfo->setEnabled(false);
                if (isPlayer)
                    ui->pause->setEnabled(true);
                else
                    ui->pause->setEnabled(false);
            }
        }
    }
}

// Обработка игровых событий
void MainWindow::GameSrtateChanged(QString newState)
{
    gameState = newState;
    ButtonsAvailability();

    repaint();
}

void MainWindow::RoleConfirmation(bool isPlayer)
{
    this->isPlayer = isPlayer;
    ButtonsAvailability();
}

void MainWindow::ConnectionStatus(bool isConnected)
{
    isConnectedToServer = isConnected;
    ButtonsAvailability();

    if (isConnectedToServer)
        StatusSignal();
}

// Отрисовка лога игровых событий
void MainWindow::FillLogList(QStringList log)
{
    ui->logList->clear();
    ui->logList->addItems(log);
    ui->logList->scrollToBottom();
}

void MainWindow::GetServerInfo(QString address, int port)
{
    if (isConnectedToServer) {
        DisconnectSignal();
    }

    serverAddress = address;
    serverPort = port;
    isConnectedToServer = false;

    ConnectSignal(address, port);
}

void MainWindow::GetPlayerInfo(QString name, QString color, bool isPlayer)
{
    playerName = name;
    playerColor = color;
    this->isPlayer = false;

    if (isConnectedToServer) {
        PlayerInfoSignal(name, color, isPlayer);
        StatusSignal();
    }
}

// Работа с локальным сервером
void MainWindow::LocalServerStart()
{
    int port = localServer.StartServer();
    if (port > 0)
        ConnectToLocalServer("127.0.0.1", port);
}

void MainWindow::LocalServerStop()
{
    localServer.StopServer();
}

void MainWindow::Initialised(Position mapSize, QList<PlayerInfo> players, int myID, int maxPlayers)
{
    // Получаем цвет от сервера
    if (isPlayer) {
        this->myID = myID;
        for (int i = 0; i < players.count(); i++)
            if (players[i].id == myID)
                this->playerColor = players[i].color;
    }

    this->maxPlayers = maxPlayers;
    this->mapSize = mapSize;
    this->players.clear();
    this->players.append(players);

    GameAreaAutosize();

    QList<DrawingInfo> toDraw = DrawInfoPreparing();
    ui->gameAreaWidget->Initialization(mapSize, toDraw);

    repaint();
}

void MainWindow::NextTurn(QList<PlayerInfo> changes)
{
    for (int i = 0; i < players.count(); i++) {
        for (int j = 0; j < changes.count(); j++) {
            if (players[i].id == changes[j].id) {
                players[i].position = changes[j].position;
                players[i].isDead = changes[j].isDead;
                players[i].tail.append(changes[j].tail);
                continue;
            }
        }
    }
    repaint();
}

// Обработка нажатия кнопок на форме
void MainWindow::on_pause_clicked()
{
    GameStateSignal();
}

void MainWindow::on_disconnectButton_clicked()
{
    DisconnectSignal();
}

void MainWindow::on_changePlayerInfo_clicked()
{
    pid->show();
}

void MainWindow::on_changeServer_clicked()
{
    csd->show();
}

