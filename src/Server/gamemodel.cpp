#include "gamemodel.h"
GameModel::GameModel(int mapWidth,int mapHeight, int max_playeres, int timer = 300, QObject *parent) : QObject(parent)
{
    this->max_players = max_playeres;
    this->mapHeight=mapHeight;
    this->mapWidth=mapWidth;
    initMap(mapHeight,mapWidth);
    initSpawns(mapHeight,mapWidth);
    qDebug()<<"Count spawns: "<<spawns.size()<<endl;
    gameState=new GameState::WaitingForStart(this);
    this->frequencyOfExecution = timer;
    if (timer == 0) {
        gameMode = new GameModes::FastMode(this);
    }
    else {
        gameMode = new GameModes::SimpleMode(this);

    }
    gameMode->setProgressFunc();
}
GameModel::~GameModel(){}

int GameModel::getNumberOfPlayers()
{
    return motorcycles.size();
}

int GameModel::getNumberOfLivePlayers()
{
    int livePlayers = 0;
    for (Motorcycle *moto: motorcycles){
        if (!moto->isDead()) livePlayers++;
    }
    return livePlayers;
}

int GameModel::getMaxPlayers()
{
    return max_players;
}

void GameModel::initMap(int mapHeight, int mapWidth){

    map.resize(mapWidth);
    for(int i = 0; i<map.size(); i++ ){
        map[i].resize(mapHeight);
        for (int j =0; j<map[i].size(); j++){
            map[i][j].type = Cell::EMPTY;
            map[i][j].location.setX(i);
            map[i][j].location.setY(j);
        }
    }
}

void GameModel::initSpawns(int mapHeight, int mapWidth)
{
    if (mapHeight < 10 || mapWidth < 10) return;

    int intervalFromEdgeUpDown = sqrt(mapWidth);
    int intervalFromEdgeLeftRight = sqrt(mapHeight);
    int intervalEdgeUpDown = (mapWidth/intervalEdge);
    int intervalEdgeLeftRight = (mapHeight/intervalEdge);
    int intervalBetweenSpawnsUpDown = sqrt(mapWidth/intervalBetweenSpawnsConst);
    int intervalBetweenSpawnsLeftRight = sqrt(mapHeight/intervalBetweenSpawnsConst);
    int countMotorcyclesInLineUpDown = (mapWidth-2*(intervalFromEdgeUpDown-1))/(intervalBetweenSpawnsUpDown);
    int countMotorcyclesInLineLeftRight = (mapHeight-2*(intervalFromEdgeLeftRight-1))/(intervalBetweenSpawnsLeftRight);
    for (int i=0; i<countMotorcyclesInLineUpDown; i++){
        spawns.push_back(new Spawn(QPoint(intervalFromEdgeUpDown+i*(intervalBetweenSpawnsUpDown+1),intervalEdgeUpDown), Direction::UP));
        spawns.push_back(new Spawn(QPoint(mapWidth - (intervalFromEdgeUpDown+i*(intervalBetweenSpawnsUpDown+1)),mapHeight-intervalEdgeUpDown-1), Direction::DOWN));
    }
    for (int i=0; i<countMotorcyclesInLineLeftRight; i++){
        spawns.push_back(new Spawn(QPoint(intervalEdgeLeftRight, intervalFromEdgeLeftRight+i*(intervalBetweenSpawnsLeftRight+1)), Direction::RIGHT));
        spawns.push_back(new Spawn(QPoint(mapWidth-intervalEdgeLeftRight-1, mapHeight - (intervalFromEdgeLeftRight+i*(intervalBetweenSpawnsLeftRight+1))), Direction::LEFT));
    }


}
//переписать если потребуется
void GameModel::initMotorcycles()
{
    QTime midnight(0,0,0);
    qsrand(midnight.secsTo(QTime::currentTime()));

    QVector<Motorcycle*> motoWhithoutSpawn = motorcycles;
    //расставление мотоциклов на карте
    for (int i = 0; i <motorcycles.size(); i++){
        Motorcycle *m = motoWhithoutSpawn[qrand() % motoWhithoutSpawn.size()];
        motoWhithoutSpawn.removeOne(m);
        m->rotateMotorcycle(spawns[i]->direction);
        m->setDenyDirection();
        m->setLocation(spawns[i]->location);
        m->destroySelf();
        m->comeToLive();
    }
}

void GameModel::setState(GameState::IGameState *state){
    this->gameState->actionBeforeDelete();
    delete this->gameState;
    this->gameState=state;
    this->gameState->onStateChanged();
    emit gameStateChange(gameState->getStateName());
}

GameState::IGameState* GameModel::getState(){
    return this->gameState;
}

void GameModel::processNextTurn(){
    updateMotorcycles();
    checkCollisions();
    sendInfoAboutNextTurn();

}

void GameModel::runGameOverTimer()
{
    if (motorcycles.size() == getMaxPlayers()){
        initMotorcycles();
        setState(new GameState::Running(this));
    }
    else{
        setState(new GameState::WaitingForStart(this));
    }



}


void GameModel::sendInfoAboutNextTurn(){
    //составление списка изменений на карте
    QVector<MotorcycleChange*> changes(motorcycles.size());
    for(int i =0; i < changes.size(); i++){
        changes[i] = new MotorcycleChange;
        changes[i]->direction = motorcycles[i]->getDirection();
        changes[i]->id = motorcycles[i]->getId();

        changes[i]->location = motorcycles[i]->getMotorcycleLocation();
        changes[i]->dead = motorcycles[i]->isDead();
        if (changes[i]->dead){
            changes[i]->trailChanges = *(motorcycles[i]->getTail());
            motorcycles[i]->destroySelf();
        }
        else
            changes[i]->trailChanges.push_back(motorcycles[i]->getLastStep());

    }
    emit nextTurn(changes);
}

void GameModel::updateMap(){

}

void GameModel::checkCollisions(){
    //сделать проверку на выход за поле
    for(Motorcycle* motorcycle: motorcycles ){
        //столкновение со стенами
        if (motorcycle->getMotorcycleLocation().x() < 0 ||
                motorcycle->getMotorcycleLocation().x() >= mapWidth ||
                motorcycle->getMotorcycleLocation().y() < 0 ||
                motorcycle->getMotorcycleLocation().y() >= mapHeight){
            //motorcycle->destroySelf();
            motorcycle->die();
            continue;
        }
        //столкновения с хвостами
        if (getCell(motorcycle->getMotorcycleLocation())->type != Cell::EMPTY){
            //motorcycle->destroySelf();
            motorcycle->die();
        }
        //столкновение головами
        for (Motorcycle* secondMoto: motorcycles){
            if (secondMoto->getId() == motorcycle->getId()) continue;
            if (motorcycle->getMotorcycleLocation() == secondMoto->getMotorcycleLocation()){
                motorcycle->die();
            }
        }
    }
}

void GameModel::updateMotorcycles(){
    Cell* head;
    //Проверку на мотоциклы, которые проиграли
    for(Motorcycle* motorcycle:motorcycles){
        if (motorcycle->isDead()) continue;
        head = getCell(motorcycle->getMotorcycleLocation());
        motorcycle->addTail(head);
        motorcycle->moveForward();
    }
}

Cell* GameModel::getCell(QPoint point){
    return &(map[point.x()][point.y()]);
}

void GameModel::addMotorcycle(Motorcycle *Motorcycle){
    motorcycles.push_back(Motorcycle);
}

void GameModel::joinRequest(QTcpSocket* socket,QString name, QString color)
{
    gameState->joinPlayer(socket, name, color);


}

void GameModel::spectateRequest(QTcpSocket *socket, Motorcycle *motorcycle)
{
    motorcycle->die();
    motorcycle->destroySelf();
    motorcycles.removeOne(motorcycle);
    if (motorcycles.size() < min_players) this->setState(new GameState::GameOverState(this));
    emit spectateAcknowledged(socket);
}

void GameModel::gameStateChangedRequest(Motorcycle *motorcycle, QString gamestate)
{
    setState(stringToIGameState(gamestate));
}

void GameModel::changeDirectionRequest(Motorcycle *motorcycle, Direction direction)
{
    if (motorcycle->getDenyDirection() != direction){
        motorcycle->rotateMotorcycle(direction);
    }
    if (!(motorcycle->isDead())){
        witness(motorcycle);
    }

    //if (gameMode->checkReadyAll()){

     //   processNextTurn();
     //   readyMotorcycle.clear();
    //}
}

void GameModel::stopLoopTimer(){
    this->mainLoopTimer->stop();
}
void GameModel::startLoopTimer(){
    this->mainLoopTimer->start();
}

void GameModel::statusRequest(QTcpSocket *socket)
{
    emit sendStatus(socket, motorcycles, gameState->getStateName(),this->mapWidth, this->mapHeight,this->max_players);
}

void GameModel::connectionLost(Motorcycle *motorcycle)
{
    qDebug()<<"Player: "<<motorcycle->getId()<<" "<<motorcycle->getName()<<" lost"<<endl;
    motorcycle->die();
    motorcycle->destroySelf();
    motorcycles.removeOne(motorcycle);
    //if (motorcycles.size() < min_players) this->setState(new GameState::GameOverState(this));
}

void GameModel::witness(Motorcycle *motorcycle)
{
    this->readyMotorcycle.insert(motorcycle->getId());
    this->gameState->checkPlayers();
}

void GameModel::mainLoop(){
    gameState->nextTurn();
}


GameState::IGameState* GameModel::stringToIGameState(QString className)
{
    if (className == "pause"){
        return new GameState::Pause(this);
    }
    else if(className == "running"){
        return new GameState::Running(this);
    }
    else{
        return new GameState::GameOverState(this);
    }
}

QString GameModel::generateColor(int number)
{
    QColor color;
    int colorOffset = 56;
    int hue=(number*colorOffset)%256;
    color.setHsl(hue,255,255/2);
    return color.name();
}

bool GameModel::checkFullLobby()
{
    if (getMaxPlayers() == getNumberOfPlayers()){
        return true;
    }
    return false;
}

bool GameModel::checkEndGame(){
    if (getNumberOfLivePlayers() < min_players) return true;
    return false;
}
