
#include "gamemode.h"
#include "gamemodel.h"

GameModes::FastMode::FastMode(GameModel *gameModel){
    this->gameModel = gameModel;
}

bool GameModes::FastMode::checkReadyAll()
{
    if (gameModel->readyMotorcycle.size() == gameModel->getMaxPlayers())
        return true;
    return false;
}

void GameModes::FastMode::joinPlayer()
{

}

void GameModes::FastMode::setProgressFunc()
{
    QObject::connect(gameModel, SIGNAL(playersMadeMove()), gameModel, SLOT(mainLoop()));

}
GameModes::SimpleMode::SimpleMode(GameModel *gameModel){
    this->gameModel = gameModel;
}

void GameModes::SimpleMode::startTimer()
{
    gameModel->startLoopTimer();
}

void GameModes::SimpleMode::stopTimer()
{
    gameModel->stopLoopTimer();
}

bool GameModes::SimpleMode::checkReadyAll()
{
    return false;
}

void GameModes::SimpleMode::setProgressFunc()
{
    gameModel->mainLoopTimer = new QTimer();
    gameModel->mainLoopTimer->setSingleShot(false);
    gameModel->mainLoopTimer->setInterval(gameModel->frequencyOfExecution);
    gameModel->mainLoopTimer->stop();
    QObject::connect(gameModel->mainLoopTimer,SIGNAL(timeout()),gameModel,SLOT(mainLoop()));
}
