#include "tcpserver.h"

TcpServer::TcpServer(int port, QObject *parent) : QObject(parent)
{
    server = new QTcpServer();
    if (!server->listen(QHostAddress::Any,port)){
        qDebug("Server could not start");
        return;
    }
    qDebug()<<"Server started at "<<server->serverAddress().toString()<<":"<<server->serverPort();
    connect(server,&QTcpServer::newConnection,this,&TcpServer::onNewConnection);
}

TcpServer::~TcpServer(){
    delete server;
}

void TcpServer::onNewConnection(){
    while(server->hasPendingConnections()){
        QTcpSocket* connectedSocket = server->nextPendingConnection();
        clients << connectedSocket;
        clientProperties[connectedSocket].isPlayer=false;
        connect(connectedSocket,SIGNAL(disconnected()),this,SLOT(onLostConnection()));
        connect(connectedSocket,SIGNAL(readyRead()),this,SLOT(onIncommingMessage()));
    }

}

void TcpServer::onLostConnection(){
    if (QTcpSocket *con = qobject_cast<QTcpSocket*>(sender())){
        if (clientProperties[con].isPlayer){
            emit connectionLost(clientProperties[con].motorcycle);
            sendPlayerLeavedMessage(clientProperties[con].motorcycle->getId());
        }
        clientProperties.remove(con);
        clients.removeOne(con);
        con->deleteLater();
    }
}

void TcpServer::onIncommingMessage(){
    if (QTcpSocket *con = qobject_cast<QTcpSocket*>(sender())){
        QByteArray incommingMessage = con->readAll();
        QByteArrayList messages = incommingMessage.split(';');
        foreach (const QByteArray& message, messages) {
            QJsonObject jsonMessage = QJsonDocument::fromJson(message).object();
            QString messageType=jsonMessage["messageType"].toString();
            //далее идет обработка типов сообщений
            if (messageType=="joinRequest"){
                QJsonObject jsonPlayer=jsonMessage["player"].toObject();
                onJoinRequest(con,jsonPlayer["name"].toString(),jsonPlayer["color"].toString());
            } else if (messageType=="spectateRequest") {
                onSpectateRequest(con);
            } else if (messageType=="statusRequest") {
                onStatusRequest(con);
            } else if (messageType=="gameStateChangeRequest"){
                onGamestateChangeRequest(con,jsonMessage["gameState"].toString());
            } else if (messageType=="changeDirectionRequest"){
                onChangeDirectionRequest(con,jsonMessage["direction"].toString());
            }
        }
    }
}

void TcpServer::sendStatus(QTcpSocket *socket, QVector<Motorcycle*> motorcycles, QString gamestate,  int mapWidth,int mapHeight, int maxPlayers){
    QJsonObject status;
    status["messageType"]="status";
    status["players"]=makePlayersJsonArray(motorcycles);
    status["gameState"]=gamestate;
    status["client"]=makeClientJson(socket);
    QJsonObject map;
    map["rows"]=mapHeight;
    map["columns"]=mapWidth;
    status["map"]=map;
    status["maxPlayers"]=maxPlayers;
    sendJsonObjectToSocket(socket,status);
}

void TcpServer::onJoinRequest(QTcpSocket *socket, QString name, QString color) {
    if (clientProperties[socket].isPlayer==true){
        return;
    }
    emit joinRequested(socket,name,color);
}

void TcpServer::onSpectateRequest(QTcpSocket *socket){
    if (clientProperties[socket].isPlayer==false){
        return;
    }
    emit spectateRequested(socket,this->clientProperties[socket].motorcycle);
}

void TcpServer::onStatusRequest(QTcpSocket *socket){
    emit statusRequested(socket);
}

void TcpServer::onGamestateChangeRequest(QTcpSocket* socket, QString gamestate)
{
    if (clientProperties[socket].isPlayer==false){
        return;
    }
    emit gamestateChangeRequested(clientProperties[socket].motorcycle,gamestate);
}

void TcpServer::onChangeDirectionRequest(QTcpSocket* socket, QString direction)
{
    if (clientProperties[socket].isPlayer==false){
        return;
    }
    emit changeDirectionRequested(clientProperties[socket].motorcycle,stringToDirection(direction));
}

void TcpServer::sendPlayerLeavedMessage(int id)
{
    QJsonObject message;
    message["messageType"]="playerLeaved";
    QJsonObject player;
    player["id"]=id;
    message["player"]=player;
    sendJsonObjectToAll(message);
}

QJsonArray TcpServer::makePlayersJsonArray(QVector<Motorcycle *> motorcycles){
    QJsonArray players;
    foreach (Motorcycle* motorcycle, motorcycles) {
        QJsonObject player=makePlayerJsonObject(motorcycle);
        players<<player;
    }
    return players;
}

QString TcpServer::directionToString(Direction direction){
    switch (direction) {
    case Direction::DOWN:
        return "down";
        break;
    case Direction::UP:
        return "up";
        break;
    case Direction::LEFT:
        return "left";
        break;
    case Direction::RIGHT:
        return "right";
        break;
    default:
        return "WRONG DIRECTION";
        break;
    }
}

Direction TcpServer::stringToDirection(QString s)
{
    if (s=="down") {
        return Direction::DOWN;
    } else if (s=="up") {
        return Direction::UP;
    } else if (s=="left") {
        return Direction::LEFT;
    } else {
        return Direction::RIGHT;
    }
}

QJsonObject TcpServer::makePlayerJsonObject(Motorcycle *motorcycle){
    QJsonObject player;
    player["id"]=motorcycle->getId();
    player["direction"]=directionToString(motorcycle->getDirection());
    QJsonObject location;
    location["x"]=motorcycle->getMotorcycleLocation().x();
    location["y"]=motorcycle->getMotorcycleLocation().y();
    player["location"]=location;
    player["color"]=motorcycle->getColor();
    player["name"]=motorcycle->getName();
    player["isDead"]=motorcycle->isDead();
    player["trail"]=makeTrailJsonArray(motorcycle->getTail());
    return player;
}

QList<QTcpSocket*> TcpServer::getClients(){
    return clients;
}

QJsonObject TcpServer::makeClientJson(QTcpSocket *socket){
    QJsonObject client;
    bool isPlayer = this->clientProperties[socket].isPlayer;
    client["isPlayer"] = isPlayer;
    if (isPlayer) {
        client["playerId"] = this->clientProperties[socket].motorcycle->getId();
    }
    return client;
}

QJsonObject TcpServer::makePlayerChangeJsonObject(MotorcycleChange *motorcycleChange)
{
    QJsonObject player;
    player["id"]=motorcycleChange->id;
    player["direсtion"]=directionToString(motorcycleChange->direction);
    QJsonObject location;
    location["x"]=motorcycleChange->location.x();
    location["y"]=motorcycleChange->location.y();
    player["location"]=location;
    player["isDead"]=motorcycleChange->dead;
    player["trailChanges"]=makeTrailChangesJsonArray(motorcycleChange->trailChanges);
    return player;
}

QJsonArray TcpServer::makeTrailChangesJsonArray(QVector<Cell *> trailChanges)
{
    QJsonArray array;
    foreach (Cell* cell, trailChanges) {
        QJsonObject change;
        change["x"]=cell->location.x();
        change["y"]=cell->location.y();
        if (cell->type==Cell::EMPTY){
            change["type"]="empty";
        } else if (cell->type==Cell::TRAIL) {
            change["type"]="trail";
        }
        array<<change;
    }
    return array;
}

QJsonArray TcpServer::makeTrailJsonArray(QVector<Cell *> *trail)
{
    QJsonArray array;
    foreach (Cell* cell, *trail) {
        QJsonObject change;
        change["x"]=cell->location.x();
        change["y"]=cell->location.y();
        array<<change;
    }
    return array;
}

void TcpServer::sendJsonObjectToSocket(QTcpSocket *socket, QJsonObject message)
{
    if (socket==nullptr || socket->state()!=QTcpSocket::ConnectedState){
        return;
    }
    QByteArray bytes = QJsonDocument(message).toJson(QJsonDocument::Compact);
    bytes.append(';');
    socket->write(bytes);
    socket->flush();
}

void TcpServer::sendJsonObjectToAll(QJsonObject message)
{
    foreach (QTcpSocket* socket, clients) {
        sendJsonObjectToSocket(socket,message);
    }
}

void TcpServer::onPlayerJoined(Motorcycle *motorcycle){
    QJsonObject message;
    message["messageType"]="playerJoined";
    message["player"]=makePlayerJsonObject(motorcycle);
    sendJsonObjectToAll(message);
}

void TcpServer::onJoinAcknowledged(QTcpSocket *socket, Motorcycle* motorcycle){
    QJsonObject jsonResponse;
    jsonResponse["messageType"]="joinAcknowledge";
    QJsonObject client;
    client["id"]=motorcycle->getId();
    jsonResponse["client"]=client;
    sendJsonObjectToSocket(socket,jsonResponse);

    clientProperties[socket].isPlayer=true;
    clientProperties[socket].motorcycle=motorcycle;
}

void TcpServer::onGameStateChange(QString gamestate)
{
    QJsonObject message;
    message["messageType"]="gameStateChanged";
    message["gameState"]=gamestate;
    sendJsonObjectToAll(message);
}

void TcpServer::onNextTurn(QVector<MotorcycleChange *> motorcycleChanges)
{
    QJsonObject message;
    message["messageType"]="nextTurn";
    QJsonArray players;
    foreach (MotorcycleChange * change, motorcycleChanges) {
        players<<makePlayerChangeJsonObject(change);
    }
    message["players"]=players;
    sendJsonObjectToAll(message);
}

void TcpServer::onSpectateAcknowledged(QTcpSocket *socket)
{
    QJsonObject jsonResponse;
    jsonResponse["messageType"]="spectateAcknowledge";
    sendJsonObjectToSocket(socket,jsonResponse);
    sendPlayerLeavedMessage(clientProperties[socket].motorcycle->getId());
    clientProperties[socket].isPlayer=false;
}
