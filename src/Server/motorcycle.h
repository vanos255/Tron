#ifndef MOTORCYCLE_H
#define MOTORCYCLE_H
#include <QVector>
#include <QObject>
#include <QtWidgets/QWidget>
#include "fwd.h"


#include "gamemodel.h"

struct RGB {
    int r;
    int g;
    int b;
};




class Motorcycle : public QObject
{
    Q_OBJECT
public:
    explicit Motorcycle(QObject *parent = 0);

    Motorcycle( Direction direction, QPoint location, QString color, int id, QString name, QObject *parent = 0);

    void moveForward();
    void rotateMotorcycle(Direction side);
    void setDenyDirection();
    void destroySelf();
    void addTail(Cell* cell);
    int getId();
    void die();
    void comeToLive();
    Cell* getLastStep();
    Direction getDirection();
    QString getColor();
    QString getName();
    QVector<Cell *> *getTail();

    bool isDead();
    QPoint getMotorcycleLocation();
    void setLocation(QPoint point);
    Direction getDenyDirection();
private:
     Direction direction;
     QPoint location;
     QString color;
     QString name;
     int id;
     bool dead;
     QVector<Cell*> *tail;
     Direction denyDirection;
     Direction getReverseDirection();
signals:

public slots:

};

#endif // MOTORCYCLE_H
