#ifndef GAMEMODE_H
#define GAMEMODE_H
#include <QString>
#include <QDebug>
#include <QTimer>
#include <QTcpSocket>
#include "fwd.h"


namespace GameModes {
class IGameMode{
protected:
    GameModel *gameModel;

public:
void virtual startTimer() = 0;
void virtual stopTimer() = 0;
bool virtual checkReadyAll() = 0;
void virtual joinPlayer() = 0;
virtual void setProgressFunc() = 0;

};


class FastMode : public IGameMode
{
public:
    FastMode(GameModel *gameModel);
    void startTimer(){}
    void stopTimer(){}
    bool checkReadyAll();
    void joinPlayer();
    void setProgressFunc();
};
class SimpleMode : public IGameMode
{
public:
    SimpleMode(GameModel *gameModel);
    void startTimer();
    void stopTimer();
    bool checkReadyAll();
    void joinPlayer(){}
    void setProgressFunc();
};
}
#endif // GAMEMODE_H
