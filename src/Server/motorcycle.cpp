#include "motorcycle.h"

Motorcycle::Motorcycle(QObject *parent) : QObject(parent)
{

}

Motorcycle::Motorcycle(Direction direction, QPoint location, QString color, int id, QString name, QObject *parent) : QObject(parent)
{

    this->direction = direction;
    this->location = location;
    this->color = color;
    this->id = id;
    this->name = name;
    tail = new QVector<Cell *>();
    this->dead = false;
    this->denyDirection = getReverseDirection();

}

void Motorcycle::addTail(Cell * c){
    c->type = Cell::TRAIL;
    tail->push_back(c);

}

void Motorcycle::moveForward(){

    switch (direction) {
    case Direction::UP:
        location.setY(location.y() + 1);
        break;
    case Direction::DOWN:
        location.setY(location.y() - 1);
        break;
    case Direction::LEFT:
        location.setX(location.x() - 1);
        break;
    case Direction::RIGHT:
        location.setX(location.x() + 1);
        break;
    default:
        break;
    }
    this->denyDirection = getReverseDirection();
}
 void Motorcycle::rotateMotorcycle(Direction side){
     this->direction = side;
 }

 void Motorcycle::setDenyDirection()
 {
     this->denyDirection = getReverseDirection();
 }
void Motorcycle::destroySelf(){
    while(!(tail->empty())){
        tail->at(0)->type = Cell::EMPTY;

        tail->pop_front();
    }
}

QPoint Motorcycle::getMotorcycleLocation(){
    return location;
}

void Motorcycle::setLocation(QPoint point)
{
    location = point;
}

Direction Motorcycle::getDenyDirection()
{
    return denyDirection;
}

Direction Motorcycle::getReverseDirection()
{
    switch (direction) {
    case Direction::UP:
        return Direction::DOWN;
        break;
    case Direction::DOWN:
        return Direction::UP;
        break;
    case Direction::LEFT:
        return Direction::RIGHT;
        break;
    case Direction::RIGHT:
        return Direction::LEFT;
        break;
    default:
        return direction;
        break;
    }
}

Direction Motorcycle::getDirection(){
    return this->direction;
}

QString Motorcycle::getColor()
{
    return this->color;
}

QString Motorcycle::getName()
{
    return this->name;
}

QVector<Cell *> *Motorcycle::getTail()
{
    return tail;
}

bool Motorcycle::isDead()
{
    return this->dead;
}

int Motorcycle::getId(){
    return this->id;
}

void Motorcycle::die()
{
    this->dead = true;
}

void Motorcycle::comeToLive()
{
    this->dead = false;
}

Cell *Motorcycle::getLastStep()
{
    return tail->at(tail->size()-1);
}
