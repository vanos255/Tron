#ifndef FWD_H
#define FWD_H
struct Cell;
enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
};
class Motorcycle;
struct MotorcycleChange;
class GameModel;

class TcpServer;

#endif // FWD_H
