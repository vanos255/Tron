#ifndef GAMESTATE_H
#define GAMESTATE_H
#include <QString>
#include <QDebug>
#include <QTimer>
#include <QTcpSocket>
#include "fwd.h"

namespace GameState {
class IGameState{
protected:
    GameModel *gameModel;

public:
    virtual void onStateChanged()=0;
    virtual QString getStateName()=0;
    virtual void nextTurn() = 0;
    virtual void actionBeforeDelete()=0;
    virtual void joinPlayer(QTcpSocket* socket,QString name, QString color) = 0;
    virtual void checkPlayers() = 0;

};

class GameOverState : public IGameState
{
public:
    GameOverState(GameModel *gameModel);
    void onStateChanged();
    QString getStateName();
    void nextTurn(){}
    void actionBeforeDelete();
    void joinPlayer(QTcpSocket* socket,QString name, QString color) {}
    void checkPlayers(){}
};

class Pause : public IGameState
{
public:
    Pause(GameModel *gameModel);
    void onStateChanged();
    QString getStateName();
    void nextTurn(){}
    void actionBeforeDelete(){}
    void joinPlayer(QTcpSocket* socket,QString name, QString color){}
    void checkPlayers(){}
};

class Running : public IGameState
{
public:
    Running(GameModel *gameModel);
    void onStateChanged();
    QString getStateName();
    void nextTurn();
    void actionBeforeDelete();
    void joinPlayer(QTcpSocket* socket,QString name, QString color){}
    void checkPlayers();
};

class WaitingForStart : public IGameState
{
public:
    WaitingForStart(GameModel *gm);
    void onStateChanged();
    QString getStateName();
    void nextTurn(){}
    void actionBeforeDelete();
    void joinPlayer(QTcpSocket* socket,QString name, QString color);
    void checkPlayers();
};
}


#endif // GAMESTATE_H
