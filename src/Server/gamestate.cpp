#include "gamestate.h"
#include "gamemodel.h"

GameState::GameOverState::GameOverState(GameModel *gameModel)
{
    this->gameModel = gameModel;
}

GameState::Pause::Pause(GameModel *gameModel)
{
    this->gameModel = gameModel;
}

GameState::Running::Running(GameModel *gameModel)
{
    this->gameModel = gameModel;
}

GameState::WaitingForStart::WaitingForStart(GameModel *gameModel)
{
    this->gameModel = gameModel;
}

void GameState::GameOverState::onStateChanged(){

    qDebug()<<"Game over"<<endl;
    if (gameModel->getNumberOfLivePlayers() == 1){
        qDebug()<<"There are winner"<<endl;
    }
    else if (gameModel->getNumberOfLivePlayers() == 0){
        qDebug()<<"There are not winner"<<endl;
    }
    else{
        qDebug()<<"The next round"<<endl;
        //gameModel->initMotorcycles();
    }
    QTimer::singleShot(timeoutAfterGameOver, gameModel, SLOT(runGameOverTimer()));

}

void GameState::Pause::onStateChanged(){

    qDebug()<<"Game paused"<<endl;
}

void GameState::Running::onStateChanged(){
    gameModel->gameMode->startTimer();
    qDebug()<<"Game started"<<endl;
}

void GameState::WaitingForStart::onStateChanged(){
    qDebug()<<"Waiting players"<<endl;
}

void GameState::GameOverState::actionBeforeDelete(){

}

void GameState::WaitingForStart::joinPlayer(QTcpSocket* socket,QString name, QString color)
{
    if (gameModel->getNumberOfPlayers() >= gameModel->getMaxPlayers() || gameModel->spawns.size() == gameModel->motorcycles.size()) {
        qDebug()<<"Too much motorcycles"<<endl;
        return;
    }
    int id = 0;//motorcycles.size();
    for (int i = 0; i<gameModel->getNumberOfPlayers(); i++){
        if (gameModel->motorcycles[i]->getId() != id) break;
        id++;
    }
    if (color.isEmpty()){
        color=gameModel->generateColor(id);
    }

    Motorcycle *newMoto = new Motorcycle(Direction::UP,QPoint(0,0), color,id , name);
    gameModel->addMotorcycle(newMoto);
    qDebug()<<"NewPlayer: "<<id<<endl;
    emit gameModel->joinAcknowledged(socket, newMoto);
    emit gameModel->GameModel::playerJoined(newMoto);

    gameModel->runGameOverTimer();
}

void GameState::WaitingForStart::checkPlayers()
{
    if (gameModel->motorcycles.size() == gameModel->max_players){
        gameModel->setState(new GameState::Running(gameModel));
    }
}


void GameState::WaitingForStart::actionBeforeDelete(){
    gameModel->initMotorcycles();
}

void GameState::Running::nextTurn(){
    gameModel->processNextTurn();
    if (gameModel->checkEndGame()){
        gameModel->setState(new GameState::GameOverState(gameModel));
    }
}

void GameState::Running::actionBeforeDelete()
{
    gameModel->gameMode->stopTimer();
}

void GameState::Running::checkPlayers()
{
    if (gameModel->readyMotorcycle.size() == gameModel->getNumberOfLivePlayers()){
        gameModel->readyMotorcycle.clear();
        emit gameModel->playersMadeMove();
    }
}




QString GameState::GameOverState::getStateName(){
    return QString("gameover");
}

QString GameState::Pause::getStateName(){
    return QString("pause");
}

QString GameState::Running::getStateName(){
    return QString("running");
}

QString GameState::WaitingForStart::getStateName(){
    return QString("lobby");
}
