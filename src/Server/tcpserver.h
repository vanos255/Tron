#ifndef TCPSERVER_H
#define TCPSERVER_H
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include "fwd.h"
#include "motorcycle.h"
#include "gamemodel.h"

struct ClientProperties{
    bool isPlayer;
    Motorcycle *motorcycle;
};

struct MotorcycleChange{
    int id;
    Direction direction;
    QPoint location;
    QVector<Cell*> trailChanges;
    bool dead;
};

class TcpServer : public QObject
{
    Q_OBJECT
public:
    explicit TcpServer(int port = 3000, QObject *parent = 0);
    ~TcpServer();

    QList<QTcpSocket*> getClients();
signals:
    void statusRequested(QTcpSocket *socket);
    void joinRequested(QTcpSocket* socket,QString name, QString color);
    void spectateRequested(QTcpSocket *socket, Motorcycle* motorcycle);
    void connectionLost(Motorcycle* motorcycle);
    void changeDirectionRequested(Motorcycle *motorcycle, Direction direction);
    void gamestateChangeRequested(Motorcycle *motorcycle, QString gamestate);

public slots:
    void sendStatus(QTcpSocket *socket, QVector<Motorcycle*> motorcycles, QString gamestate,int mapHeight,int mapWidth, int maxPlayers);
    void onJoinAcknowledged(QTcpSocket *socket, Motorcycle* motorcycle);
    void onPlayerJoined(Motorcycle* motorcycle);
    void onGameStateChange(QString gamestate);
    void onNextTurn(QVector<MotorcycleChange*> motorcycleChanges);
    void onSpectateAcknowledged(QTcpSocket* socket);

private slots:
    void onNewConnection();
    void onLostConnection();
    void onIncommingMessage();
private:
    QTcpServer * server;
    QList<QTcpSocket*> clients;
    QHash<QTcpSocket*, ClientProperties> clientProperties;

    void onJoinRequest(QTcpSocket* socket, QString name, QString color);
    void onSpectateRequest(QTcpSocket* socket);
    void onStatusRequest(QTcpSocket* socket);
    void onGamestateChangeRequest(QTcpSocket* socket, QString gamestate);
    void onChangeDirectionRequest(QTcpSocket* socket, QString direction);

    void sendPlayerLeavedMessage(int id);

    QJsonArray makePlayersJsonArray(QVector<Motorcycle*> motorcycles);
    QJsonObject makePlayerJsonObject(Motorcycle* motorcycle);
    QJsonObject makeClientJson(QTcpSocket* socket);
    QJsonObject makePlayerChangeJsonObject(MotorcycleChange* motorcycleChange);
    QJsonArray makeTrailChangesJsonArray(QVector<Cell*> trailChanges);
    QJsonArray makeTrailJsonArray(QVector<Cell*> *trail);

    void sendJsonObjectToSocket(QTcpSocket* socket, QJsonObject message);
    void sendJsonObjectToAll(QJsonObject message);

    QString directionToString(Direction direction);
    Direction stringToDirection(QString s);
};
#endif // TCPSERVER_H
