#include "fwd.h"
#include "gamemodel.h"
#include "tcpserver.h"
#include "QCoreApplication"
#include <QCommandLineParser>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc,argv);

    QCommandLineParser parser;
    parser.addHelpOption();

    parser.addOptions({
            {{"r", "rows"},
                QCoreApplication::translate("main", "Number of rows on map. Default=10"),
                QCoreApplication::translate("main", "rows")},
            {{"c", "columns"},
                QCoreApplication::translate("main", "Number of columns on map. Default=10"),
                QCoreApplication::translate("main", "columns")},
            {{"p", "players"},
                QCoreApplication::translate("main", "Number of players. Default=2"),
                QCoreApplication::translate("main", "players")},
            {{"o", "port"},
                QCoreApplication::translate("main", "Port of server. Default=3000"),
                QCoreApplication::translate("main", "port")},
            {{"t", "timer"},
                QCoreApplication::translate("main", "Timе between turns. 0 is for fast mode. Default=300"),
                QCoreApplication::translate("main", "timer")}
        });

    parser.process(a);

    //значения по умолчанию
    int columns = 30;
    int rows = 40;
    int players = 2;
    int port = 3000;
    int timer = 300;

    if (parser.isSet("rows")){
        rows = parser.value("rows").toInt();
    }

    if (parser.isSet("columns")){
        columns = parser.value("columns").toInt();
    }

    if (parser.isSet("players")){
        players = parser.value("players").toInt();
    }

    if (parser.isSet("port")){
        port = parser.value("port").toInt();
    }

    if (parser.isSet("timer")){
        timer = parser.value("timer").toInt();
    }

    TcpServer tcpserver(port);
    GameModel gamemodel(columns,rows, players, timer);
    QObject::connect(&tcpserver,SIGNAL(statusRequested(QTcpSocket*)),&gamemodel,SLOT(statusRequest(QTcpSocket*)));
    QObject::connect(&tcpserver,SIGNAL(joinRequested(QTcpSocket*,QString,QString)),&gamemodel,SLOT(joinRequest(QTcpSocket*,QString,QString)));
    QObject::connect(&gamemodel,SIGNAL(sendStatus(QTcpSocket*,QVector<Motorcycle*>,QString,int,int,int)),&tcpserver,SLOT(sendStatus(QTcpSocket*,QVector<Motorcycle*>,QString,int,int,int)));
    QObject::connect(&gamemodel,SIGNAL(joinAcknowledged(QTcpSocket*,Motorcycle*)),&tcpserver,SLOT(onJoinAcknowledged(QTcpSocket*,Motorcycle*)));
    QObject::connect(&tcpserver,SIGNAL(spectateRequested(QTcpSocket*,Motorcycle*)),&gamemodel,SLOT(spectateRequest(QTcpSocket*,Motorcycle*)));
    QObject::connect(&gamemodel,SIGNAL(spectateAcknowledged(QTcpSocket*)),&tcpserver,SLOT(onSpectateAcknowledged(QTcpSocket*)));
    QObject::connect(&gamemodel,SIGNAL(playerJoined(Motorcycle*)),&tcpserver,SLOT(onPlayerJoined(Motorcycle*)));
    QObject::connect(&gamemodel, SIGNAL(nextTurn(QVector<MotorcycleChange*>)),&tcpserver, SLOT(onNextTurn(QVector<MotorcycleChange*>)));
    QObject::connect(&tcpserver,SIGNAL(connectionLost(Motorcycle*)), &gamemodel, SLOT(connectionLost(Motorcycle*)));
    QObject::connect(&tcpserver, SIGNAL(changeDirectionRequested(Motorcycle*,Direction)),&gamemodel,SLOT(changeDirectionRequest(Motorcycle*,Direction)));
    QObject::connect(&tcpserver, SIGNAL(gamestateChangeRequested(Motorcycle*,QString)), &gamemodel, SLOT(gameStateChangedRequest(Motorcycle*,QString)));
    QObject::connect(&gamemodel,SIGNAL(gameStateChange(QString)),&tcpserver,SLOT(onGameStateChange(QString)));



    a.exec();
}
