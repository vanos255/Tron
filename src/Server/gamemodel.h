#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include <QVector>
#include <QObject>
#include <QPoint>
#include <QTimer>
#include <QTime>
#include <QDebug>
#include <QColor>
#include <qmath.h>
#include "tcpserver.h"
#include "fwd.h"

#include "gamestate.h"
#include "gamemode.h"



#include "motorcycle.h"
//setting const

const int intervalFromEdgeConst = 5;
const int intervalBetweenSpawnsConst = 7;
const int intervalEdge = 10;

const int min_players = 2;
//const int max_players = 3;
const int timeoutAfterGameOver = 10000;
//const int frequencyOfExecution = 300;

struct Cell{
    QPoint location;
    enum Type {EMPTY, TRAIL, MOTORCYCLE} type;
};

struct Spawn{
    QPoint location;
    Direction direction;
    Spawn(QPoint location, Direction direction){
        this->location = location;
        this->direction = direction;
    }
};
class GameModel : public QObject
{
    Q_OBJECT
public:
    friend class GameState::WaitingForStart;
    friend class GameState::Running;
    friend class GameModes::FastMode;
    friend class GameModes::SimpleMode;
    explicit GameModel(int mapWidth, int mapHeight, int max_players, int timer, QObject *parent = 0);
    ~GameModel();
    bool checkEndGame();
    void setState(GameState::IGameState *state);
    void setServer(QTcpServer *server);
    GameState::IGameState* getState();
    void addMotorcycle(Motorcycle* Motorcycle);
    void startMainLoop();
    int getNumberOfPlayers();
    int getNumberOfLivePlayers();
    int getMaxPlayers();
    void initMotorcycles();

    void stopLoopTimer();
    void startLoopTimer();
signals:
    void spectateAcknowledged(QTcpSocket* socket);
    void joinAcknowledged(QTcpSocket *socket, Motorcycle* motorcycle);
    void sendStatus(QTcpSocket *socket, QVector<Motorcycle*> motorcycles, QString gamestate,int width, int heght, int maxPlayers);
    void playerJoined(Motorcycle* motorcycle);
    void gameStateChange(QString gamestate);
    void nextTurn(QVector<MotorcycleChange*> motorcycleChanges);
    void playersMadeMove();

public slots:
    void joinRequest(QTcpSocket* socket,QString name, QString color);
    void spectateRequest(QTcpSocket *socket, Motorcycle* motorcycle);
    void gameStateChangedRequest(Motorcycle *motorcycle, QString gamestate);
    void changeDirectionRequest(Motorcycle *motorcycle, Direction direction);
    void statusRequest(QTcpSocket *socket);
    void connectionLost(Motorcycle* motorcycle);



    void mainLoop();
    void processNextTurn();

    void runGameOverTimer();

private:
    int frequencyOfExecution;
    int max_players;
    QTimer *mainLoopTimer;
    GameModes::IGameMode *gameMode;
    GameState::IGameState *gameState;
    QVector<QVector<Cell> > map;
    QVector<Spawn*> spawns;
    int mapHeight;
    int mapWidth;
    Cell* getCell(QPoint point);

    QVector<Motorcycle*> motorcycles;
    QSet<int> readyMotorcycle;
    void sendInfoAboutNextTurn();
    void updateMap();
    void checkCollisions();
    void initMap(int mapHeight, int mapWidth);
    void initSpawns(int mapHeight, int mapWidth);
    void updateMotorcycles();

    GameState::IGameState* stringToIGameState(QString className);

    QString generateColor(int number);
    bool checkFullLobby();
    void witness(Motorcycle *motorcycle);

};

#endif // GAMEMODEL_H
