#ifndef TESTCLASS_H
#define TESTCLASS_H
#include "../../../src/Server/gamemodel.h"

class testClass : public QObject
{
    Q_OBJECT
public:
    explicit testClass(QObject *parent = 0);

    QVector<MotorcycleChange*> info;
signals:

public slots:
    void nextTurn(QVector<MotorcycleChange*> motorcycleChanges);
};

#endif // TESTCLASS_H
