#include <QString>
#include <QtTest>
#include "../../../src/Server/fwd.h"
#include "../../../src/Server/gamemodel.h"
#include "testclass.h"

class GameModelTest : public QObject
{
    Q_OBJECT

public:
    GameModelTest();

private Q_SLOTS:
    void testCase1();
    void testNextTurn();
    void testSetState();
    void testSignalNextTurn();

};

GameModelTest::GameModelTest()
{
}

void GameModelTest::testCase1()
{
    QVERIFY2(true, "Failure");
}

void GameModelTest::testNextTurn(){
    GameModel *gm = new GameModel(15, 15, 2, 0);
    gm->addMotorcycle(new Motorcycle(Direction::UP, QPoint{0,0},QString("red"), 0, QString("test")));
    gm->addMotorcycle(new Motorcycle(Direction::LEFT, QPoint{2,1},QString("red"), 1, QString("test")));
    gm->processNextTurn();

    bool end = false;
    if (gm->getNumberOfLivePlayers() != 2) end = true;
    QCOMPARE(false, end);
    gm->processNextTurn();

    end = gm->checkEndGame();
    QCOMPARE(true, end);
    delete gm;
}
void GameModelTest::testSetState(){

    GameModel *gm2 = new GameModel(15,15,2,0);
    Motorcycle *moto = new Motorcycle(Direction::UP, QPoint{0,0},QString("red"), 0, QString("test"));
    gm2->addMotorcycle(moto);
    //QSignalSpy(gm2, SIGNAL(sendStatus(QTcpSocket*,QVector<Motorcycle*>,QString)));
    gm2->gameStateChangedRequest(moto, QString("running"));

    QString exceptState = gm2->getState()->getStateName();

    QCOMPARE(QString("running"), exceptState);

    gm2->gameStateChangedRequest(moto, QString("pause"));

    exceptState = gm2->getState()->getStateName();

    QCOMPARE(QString("pause"), exceptState);
}

void GameModelTest::testSignalNextTurn(){
    GameModel *gm3 = new GameModel(15, 15, 2, 0);
    gm3->addMotorcycle(new Motorcycle(Direction::UP, QPoint{0,0},QString("red"), 0, QString("test")));
    gm3->addMotorcycle(new Motorcycle(Direction::LEFT, QPoint{2,1},QString("red"), 1, QString("test")));


    testClass t;
    connect(gm3, SIGNAL(nextTurn(QVector<MotorcycleChange*>)),&t, SLOT(nextTurn(QVector<MotorcycleChange*>)));
    gm3->processNextTurn();

    QCOMPARE(t.info[0]->id, 0);
    QCOMPARE(t.info[0]->trailChanges[0]->location, QPoint(0,0));
    QCOMPARE(t.info[1]->trailChanges[0]->location, QPoint(2,1));
    gm3->processNextTurn();

    QCOMPARE(t.info[1]->id, 1);
    QCOMPARE(t.info[1]->dead, true);



    delete gm3;

}
QTEST_APPLESS_MAIN(GameModelTest)

#include "gamemodeltest.moc"
