#include <QString>
#include <QtTest>
#include <QTcpSocket>
#include <QSignalSpy>
#include "../../../src/Server/tcpserver.h"
class NetworkTest : public QObject
{
    Q_OBJECT

public:
    NetworkTest();

private Q_SLOTS:
    void serverStarts();
    void send_status_after_call_getStatus();
    void status_sends_correct_players();
    void status_sends_correct_gamestate();
    void clientCanJoinAsPlayer();
    void playerCanBecomeSpectator();
};

NetworkTest::NetworkTest()
{
}

QJsonObject objectFromBytes(QByteArray& bytes){
    return QJsonDocument::fromJson(bytes).object();
}

QByteArray joinPlayer(QTcpSocket* socket, const QString& name, const QString& color = "#ffffff"){
    QString req("{\"messageType\" : \"joinRequest\",\"player\" : {\"name\" : \""+name+"\",\"color\" : \"#ffffff\"}}");
    socket->write(req.toUtf8());
    QTest::qWait(100);
    QByteArray answer = socket->readAll();
    return answer;
}

QByteArray spectateReq(QTcpSocket* socket){
    QString req("{\"messageType\" : \"spectateRequest\"}");
    socket->write(req.toUtf8());
    QTest::qWait(100);
    QByteArray answer = socket->readAll();
    return answer;
}

void NetworkTest::serverStarts(){
    TcpServer server(3000);
    QTcpSocket socket;
    socket.connectToHost(QHostAddress("127.0.0.1"),3000);
    QTest::qWait(100);
    socket.waitForConnected(1000);
    QCOMPARE(socket.state(), QTcpSocket::ConnectedState);
}

QVector<Motorcycle*> oneMotoVector(Direction direction, int x, int y){
    QVector<Motorcycle*> vec;
    vec<<new Motorcycle(direction,QPoint{x,y},QString("red"),0, QString("test"));
    return vec;
}

void NetworkTest::send_status_after_call_getStatus(){
    TcpServer server(3000);
    QTcpSocket socket;
    socket.connectToHost(QHostAddress("127.0.0.1"),3000);
    QTest::qWait(100);
    server.sendStatus(server.getClients().last(),oneMotoVector(Direction::UP,0,0),"running",4,4,2);
    QTest::qWait(100);
    QByteArray answer = socket.readAll();
    answer=answer.split(';').first();
    QString answerMessageType = objectFromBytes(answer)["messageType"].toString();

    QCOMPARE(answerMessageType,QString("status"));
}

void NetworkTest::status_sends_correct_players(){
    TcpServer server(3000);
    QTcpSocket socket;
    socket.connectToHost(QHostAddress("127.0.0.1"),3000);
    QTest::qWait(100);
    server.sendStatus(server.getClients().last(),oneMotoVector(Direction::UP,0,0),"running",4,4,2);
    QTest::qWait(100);
    QByteArray answer = socket.readAll();

    QByteArray expectedByteArray("[{\"id\" : 1,\"name\" : \"player 1\",\"direction\" : \"up\",\"location\" : {\"x\" : 0,\"y\" : 1},\"color\" : \"#ffffff\",\"isDead\" : false,\"trail\" : [{\"x\":0,\"y\":0}]}]");
    QJsonObject expected = objectFromBytes(expectedByteArray);

    QCOMPARE(objectFromBytes(answer)["players"].toObject(),expected);
}

void NetworkTest::status_sends_correct_gamestate(){
    TcpServer server(3000);
    QTcpSocket socket;
    socket.connectToHost(QHostAddress("127.0.0.1"),3000);
    QTest::qWait(100);
    server.sendStatus(server.getClients().last(),oneMotoVector(Direction::UP,0,0),"running",4,4,2);
    QTest::qWait(100);
    QByteArray answer = socket.readAll();
    answer=answer.split(';').first();
    QString answerGameState = objectFromBytes(answer)["gameState"].toString();

    QCOMPARE(answerGameState,QString("running"));
}

void NetworkTest::clientCanJoinAsPlayer(){
    TcpServer server(3000);
    QTcpSocket socket;
    socket.connectToHost(QHostAddress("127.0.0.1"),3000);
    QTest::qWait(100);
    QByteArray answer = joinPlayer(&socket,"player 1");

    QString answerMessageType = objectFromBytes(answer)["messageType"].toString();
    //QCOMPARE(answerMessageType,QString("joinAcknowledge"));
}

void NetworkTest::playerCanBecomeSpectator(){
    TcpServer server(3000);
    QTcpSocket socket;
    socket.connectToHost(QHostAddress("127.0.0.1"),3000);
    QTest::qWait(100);
    QByteArray answer;

    answer = joinPlayer(&socket,"player 1");
    answer = spectateReq(&socket);
    QString answerMessageType = objectFromBytes(answer)["messageType"].toString();
    //QCOMPARE(answerMessageType,QString("spectateAcknowledge"));
}

QTEST_GUILESS_MAIN(NetworkTest)

#include "networktest.moc"
