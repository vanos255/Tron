#include <QString>
#include <QtTest>
//#include <QtWidgets/QWidget>
#include "../../../src/Server/motorcycle.h"
#include <QColor>

class MotorcycleTest : public QObject
{
    Q_OBJECT

public:
    MotorcycleTest();

private Q_SLOTS:
    void testCase1();
    void testRotateMotorcycle();
    void testMoveForward();
};

MotorcycleTest::MotorcycleTest()
{
}

void MotorcycleTest::testCase1()
{
    QVERIFY2(true, "Failure");
}

void MotorcycleTest::testRotateMotorcycle()
{
    Motorcycle *m = new Motorcycle(Direction::UP, QPoint{0, 0}, QString("red"), 0, QString("test"));
    m->rotateMotorcycle(Direction::RIGHT);
    QCOMPARE(m->getDirection(), Direction::RIGHT);
    m->rotateMotorcycle(Direction::DOWN);
    QCOMPARE(m->getDirection(), Direction::DOWN);
    m->rotateMotorcycle(Direction::LEFT);
    QCOMPARE(m->getDirection(), Direction::LEFT);
    m->rotateMotorcycle(Direction::UP);
    QCOMPARE(m->getDirection(), Direction::UP);

    delete m;
}

void MotorcycleTest::testMoveForward(){
    Motorcycle *m2 = new Motorcycle(Direction::UP, QPoint{0, 0}, QString("red"), 1,  QString("test"));
    m2->moveForward();
    QCOMPARE(m2->getMotorcycleLocation(), QPoint(0,1));
    m2->rotateMotorcycle(Direction::RIGHT);
    m2->moveForward();
    QCOMPARE(m2->getMotorcycleLocation(), QPoint(1,1));
    m2->rotateMotorcycle(Direction::DOWN);
    m2->moveForward();
    QCOMPARE(m2->getMotorcycleLocation(), QPoint(1,0));
    m2->rotateMotorcycle(Direction::LEFT);
    m2->moveForward();
    QCOMPARE(m2->getMotorcycleLocation(), QPoint(0,0));

    delete m2;

}

QTEST_APPLESS_MAIN(MotorcycleTest)

#include "motorcycletest.moc"
