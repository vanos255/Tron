#-------------------------------------------------
#
# Project created by QtCreator 2017-03-16T18:01:41
#
#-------------------------------------------------

QT       += testlib

QT       += gui

QT       += network

TARGET = motorcycletest
CONFIG   += console testcase
CONFIG   -= app_bundle
CONFIG +=warn_off

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += motorcycletest.cpp \
    ../../../src/Server/motorcycle.cpp \
    ../../../src/Server/gamemodel.cpp \
    ../../../src/Server/gamestate.cpp \
    ../../../src/Server/gamemode.cpp
HEADERS += ../../../src/Server/motorcycle.h \
    ../../../src/Server/fwd.h \
    ../../../src/Server/gamemodel.h \
    ../../../src/Server/gamestate.h \
    ../../../src/Server/gamemode.h
DEFINES += SRCDIR=\\\"$$PWD/\\\"


include( ../../../common.pri )
include( ../../../app.pri )
